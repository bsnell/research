from random import randrange
import cocotb
from cocotb.triggers import Timer, FallingEdge
from cocotb.clock import Clock
from cocotb.regression import TestFactory
import pytest

async def write_data(dut, addr: int, data: int ):
    dut.i_waddr.value = addr
    dut.i_wdata.value = data
    dut.i_wen.value = 1

    await Timer(1, units="ns")

    dut.i_wen.value = 0

async def read_data(dut, addr: int)->int:
    dut.i_raddr = addr
    dut.i_ren.value = 1

    await Timer(1, units="ns")

    dut.i_ren.value = 0
    return dut.o_rdata.value


@cocotb.test()
async def incrementing(dut):
    """Write incrementing values to the entire memory range and read them back"""

    cocotb.start_soon(Clock(dut.clk, 1, units="ns").start())

    dut.rst.value = 0

    await Timer(5, units="ns")
    await FallingEdge(dut.clk)
    
    for i in range(32):
        await write_data(dut, i, i)
        resp = await read_data(dut, i)
        
        assert resp == i

@cocotb.test()
async def zeros(dut):
    """Write incrementing values to the entire memory range and read them back"""

    cocotb.start_soon(Clock(dut.clk, 1, units="ns").start())

    dut.rst.value = 0

    await Timer(5, units="ns")
    await FallingEdge(dut.clk)
    
    data = 0
    for i in range(32):
        await write_data(dut, i, data)
        resp = await read_data(dut, i)
        
        assert resp == data

@cocotb.test()
async def ones(dut):
    """Write incrementing values to the entire memory range and read them back"""

    cocotb.start_soon(Clock(dut.clk, 1, units="ns").start())

    dut.rst.value = 0

    await Timer(5, units="ns")
    await FallingEdge(dut.clk)
    
    data = 0xFFFFFFFF
    for i in range(32):
        await write_data(dut, i, data)
        resp = await read_data(dut, i)
        
        assert resp == data

@cocotb.test()
async def random(dut):
    """Write incrementing values to the entire memory range and read them back"""

    cocotb.start_soon(Clock(dut.clk, 1, units="ns").start())

    dut.rst.value = 0

    await Timer(5, units="ns")
    await FallingEdge(dut.clk)
    
    for i in range(32):
        data = randrange(0, 0xFFFFFFFF)
        await write_data(dut, i, data)
        resp = await read_data(dut, i)
        
        assert resp == data

@cocotb.test()
async def reset(dut):
    """Test that the registers reset correctly"""

    cocotb.start_soon(Clock(dut.clk, 1, units="ns").start())

    # Fill with random data
    dut.rst.value = 0
    await Timer(5, units="ns")
    await FallingEdge(dut.clk)

    for i in range(32):
        data = randrange(0, 0xFFFFFFFF)
        await write_data(dut, i, data)
        resp = await read_data(dut, i)
        
        assert resp == data

    dut.rst.value = 1
    await Timer(5, units="ns")
    dut.rst.value = 0
    
    for i in range(32):
        resp = await read_data(dut, i)
        
        assert resp == 0
