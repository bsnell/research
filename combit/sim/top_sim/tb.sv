`timescale 1ns/10ps

module top_tb;

reg clk;
reg cpu_rst;
reg imem_rst;
reg dmem_rst;

combit_top DUT(.clk(clk), .cpu_rst(cpu_rst), .imem_rst(imem_rst), .dmem_rst(dmem_rst));

always
begin
    clk = 1'b1;
    #1;
    clk = 1'b0;
    #1;
end

always @(posedge clk)
begin
    // Put everything into reset
    cpu_rst = 1;
    imem_rst = 1;
    dmem_rst = 1;
    #1;

    DUT.imem.mem[0] = 1'b1;
    #1;
end

endmodule