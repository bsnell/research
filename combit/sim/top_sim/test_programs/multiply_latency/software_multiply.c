int software_multiply(int a, int b) {
    int result = 0;
    while (b > 0) {
        if (b & 1) {
            result += a;
        }
        
        a = a << 1;
        b = b >> 1;
   }
   return result;
}