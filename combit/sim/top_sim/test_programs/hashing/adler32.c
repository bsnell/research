#include <stdint.h>
#include <stdio.h>
const uint32_t MOD_ADLER = 65521;

int main(unsigned char *data, size_t len)
{   
    uint32_t a = 1, b = 0;
    size_t index;
    
    // Process each byte of the data in order
    for (index = 0; index < len; ++index)
    {
        a = (a + data[index]) % MOD_ADLER;
        b = (b + a) % MOD_ADLER;
    }
    
    return (b << 16) | a;
}