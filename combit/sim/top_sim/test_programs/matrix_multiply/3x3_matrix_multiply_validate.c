// #include <stdio.h>
// #include <stdint.h>

int software_multiply(int a, int b) {
    int result = 0;
    while(b > 0) {
        if(b & 1 == 1) {
            result += a;
        }

        a = (a << 1);
        b = (int)((unsigned int) b >> 1);
    }
    return result;
}

int main()
{   

    // int mat1 [9] = {1, 2, 3, 1, 2, 3, 1, 2, 3};
    // int mat2 [9] = {1, 1, 1, 2, 2, 2, 3, 3, 3};
    // int res [9];
    // int sum;

    volatile unsigned int *mat1 = (volatile unsigned int *) 0x0;
    volatile unsigned int *mat2 = (volatile unsigned int *) 0x24;
    volatile unsigned int *res = (volatile unsigned int *) 0x48;
    int * sum = (int *)0x6C;

    for (int i=0; i<3; i++) {
        for (int j=0; j<3; j++) {
            
            sum = 0;
            for (int k=0; k < 3; k++) {
                // sum += mat1[i*3 + k] * mat2[k*3+j];
                sum += software_multiply(mat1[i*3 + k], mat2[k*3+j]);
            }
            res[i*3+j] = *sum;
            // printf("%d ", sum);
        }
    }

    mat1[0] = 0;

    return 0;
}
