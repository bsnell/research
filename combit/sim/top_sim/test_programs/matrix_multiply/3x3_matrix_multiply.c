int main()
{   

    int * mat1 = (int*) 0x0;
    int * mat2 = (int*) 0x24;
    int * res = (int*) 0x48;
    int * sum = (int*) 0x6C;

    for (int i=0; i<3; i++) {
        for (int j=0; j<3; j++) {
            
            *sum = 0;
            for (int k=0; k < 3; k++) {
                sum += mat1[i*3 + k] * mat2[k*3+j];
            }
            res[i*3+j] = *sum;
        }
    }

    mat1[0] = 0;

    return 0;
}
