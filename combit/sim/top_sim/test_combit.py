import curses
import shutil
from typing import List
import zlib
import cocotb
from cocotb.triggers import Timer, FallingEdge, ClockCycles, RisingEdge
from cocotb.clock import Clock
from riscv_assembler.convert import AssemblyConverter as AC
import subprocess
import os
import re
from dataclasses import dataclass, field
from typing import List, Tuple
import pandas as pd
import numpy as np
import string

LITTLE_IMEM_OFFSET = 0x0
BIG_IMEM_OFFSET = 0x0

DATA_DIR = "./data/"

MOD_ADLER = 8

BASE_PATH = "/opt/riscv32im/bin/riscv32-unknown-linux-gn"
ASSEMBLER_PATH = "/opt/riscv32im/bin/riscv32-unknown-elf-as"
OBJDUMP_PATH = "/opt/riscv32im/bin/riscv32-unknown-elf-objdump"

def assemble_program(program: list[str]):
    with open("temp.s", "w+") as f:
        for line in program:
            f.write(line + "\n")

    subprocess.run(["/opt/riscv32im/bin/riscv32-unknown-elf-as", "-march=rv32im", "-o", "temp.o", "temp.s"])

    process = subprocess.run(["/opt/riscv32im/bin/riscv32-unknown-elf-objdump", "-d", "temp.o"], capture_output=True, text=True)
    lines = process.stdout.split("\n")
    data = []
    pattern = r"[0-9A-Fa-f]+:\s*([0-9A-Fa-f]*)"
    for line in lines:
        match = re.search(pattern, line)
        if match is not None:
            data.append(int(match.group(1), 16))
    
    os.remove("temp.s")
    return data

def compile_c(path: str):
    subprocess.run(["/opt/riscv32im/bin/riscv32-unknown-elf-gcc", "-O0", "-march=rv32im", path, "-o", "temp.o"])

    process = subprocess.run(["/opt/riscv32im/bin/riscv32-unknown-elf-objdump", "-d", "temp.o"], capture_output=True, text=True)
    lines = process.stdout.split("\n")
    data = []
    pattern = r"[0-9A-Fa-f]+:\s*([0-9A-Fa-f]*)"
    for line in lines:
        match = re.search(pattern, line)
        if match is not None:
            data.append(int(match.group(1), 16))
    
    os.remove("temp.o")
    return data

def extend_imem(program: List[int], length: int=65536, extension_data: int=0)->List[int]:
    result = program.copy()
    extension = [extension_data] * (length-len(program))
    result.extend(extension)
    return result


def cpu_reset(dut, enable: bool):
    dut.cpu_rst.value = enable

def imem_reset(dut, enable: bool):
    dut.imem_rst.value = enable

def dmem_reset(dut, enable: bool):
    dut.dmem_rst.value = enable

def init_dmem(dut):
    force_dmem(dut, 0, [0]*16384)


def force_imem(dut, target, address, data):
    for i, d in enumerate(data):
        if target.lower() == "little":
            dut.little_imem.mem[address+i].value = d
        else:
            dut.big_imem.mem[address+i].value = d

def force_dmem(dut, address, data):
    for i, d in enumerate(data):
        dut.dmem.mem[address+i].value = d
        

def read_dmem(dut, address:int, count:int=1)->List[int]:
    """Returns data as list of 4 bytes"""
    raw_dmem = dut.dmem.mem.value[::-1]
    return raw_dmem[address:address+count]
    # result_data = [raw_dmem[i+0] + raw_dmem[i+1] + raw_dmem[i+2] + raw_dmem[i+3] for i in range(0, count*4, 4)]
    # return result_data

def program_printer(program):
    for i in program:
        print(i)

def machine_code_printer(machine_code: List[int]):
    for i in machine_code:
        print(f"{i:08x}")

def calculate_adler32(test_string: str, table_size: int = MOD_ADLER):
    a = 1
    b = 0
    data = [ord(c) for c in test_string]
    for i in range(len(test_string)):
        a = (a+data[i]) % table_size
        b = (b+a) % table_size

    return (b<<16) | a

def calculate_adler32_int(data: List[int], table_size: int = MOD_ADLER):
    a = 1
    b = 0
    for i in range(len(data)):
        a = (a+data[i]) % table_size
        b = (b+a) % table_size

    return (b<<16) | a

@dataclass
class mult_test:
    operand_a: int
    operand_b: int
    expected: int = field(init=False)

    def __post_init__(self):
        self.expected = (self.operand_a * self.operand_b) & 0xFFFF_FFFF

    def __repr__(self) -> str:
        data = ""
        data += f"OP A: 0x{self.operand_a:08x}, "
        data += f"OP B: 0x{self.operand_b:08x}, "
        data += f"Expected: 0x{self.expected:08x}"
        return data

@cocotb.test(skip=True)
async def little_alive(dut):
    """Test that the little core is alive"""

    cocotb.start_soon(Clock(dut.clk, 1, units="ns").start())

    # Put cpus and memory in reset
    cpu_reset(dut, True)
    imem_reset(dut, True)
    dmem_reset(dut, True)
    init_dmem(dut)

    await RisingEdge(dut.clk)
    await ClockCycles(dut.clk, 1)

    program = [
        "addi x5, x0, 1",
        "sw x5, 0(x0)"
    ]

    machine_code = assemble_program(program)

    imem_reset(dut, False)
    
    force_imem(dut, "little", LITTLE_IMEM_OFFSET, machine_code)
    await ClockCycles(dut.clk, 2)
    
    
    dmem_reset(dut, False)
    await ClockCycles(dut.clk, 10)
    
    cpu_reset(dut, False)
    await ClockCycles(dut.clk, 25)
    
    dmem_value = read_dmem(dut, 0, 1)[0]
    assert dmem_value == 1


@cocotb.test(skip=True)
async def big_alive(dut):
    """Test that the big core is alive"""

    cocotb.start_soon(Clock(dut.clk, 1, units="ns").start())

    # Put cpus and memory in reset
    cpu_reset(dut, True)
    imem_reset(dut, True)
    dmem_reset(dut, True)
    init_dmem(dut)

    await RisingEdge(dut.clk)
    await ClockCycles(dut.clk, 1)

    program = [
        "addi x5, x0, 1",
        "sw x5, 0(x0)"
    ]

    machine_code = assemble_program(program)
    imem_reset(dut, False)
    force_imem(dut, "big", BIG_IMEM_OFFSET, machine_code)

    await ClockCycles(dut.clk, 2)
    
    
    dmem_reset(dut, False)
    await ClockCycles(dut.clk, 10)
    
    cpu_reset(dut, False)
    await ClockCycles(dut.clk, 7)
    
    dmem_value = read_dmem(dut, 0, 1)[0]
    assert dmem_value == 1


@cocotb.test(skip=True)
async def big_multiply(dut):
    """Test that the big core can handle a multiply instruction"""

    cocotb.start_soon(Clock(dut.clk, 1, units="ns").start())

    # Put cpus and memory in reset
    cpu_reset(dut, True)
    imem_reset(dut, True)
    dmem_reset(dut, True)
    init_dmem(dut)

    await RisingEdge(dut.clk)
    await ClockCycles(dut.clk, 1)

    program = [
        "addi x5, x0, 2",
        "addi x6, x0, 2",
        "mul x7, x6, x5",
        "sw x7, 0(x0)"
    ]

    machine_code = assemble_program(program)
    
    imem_reset(dut, False)
    force_imem(dut, "big", BIG_IMEM_OFFSET, machine_code)

    await ClockCycles(dut.clk, 2)
    
    
    dmem_reset(dut, False)
    await ClockCycles(dut.clk, 10)
    
    cpu_reset(dut, False)
    await ClockCycles(dut.clk, 13)
    
    dmem_value = read_dmem(dut, 0, 1)[0]
    assert dmem_value == 4


@cocotb.test(skip=True)
async def big_lw(dut):
    """Test that the big core can load value from memory"""

    cocotb.start_soon(Clock(dut.clk, 1, units="ns").start())

    # Put cpus and memory in reset
    cpu_reset(dut, True)
    imem_reset(dut, True)
    dmem_reset(dut, True)
    init_dmem(dut)

    await RisingEdge(dut.clk)
    await ClockCycles(dut.clk, 1)

    program = [
        "xor x0, x0, x0",
        "lw x1, 0(x0)",
        "lw x2, 4(x0)",
        "lw x3, 8(x0)",
        "add x1, x1, x1",
        "add x2, x2, x2",
        "add x3, x3, x3",
        "sw x1, 0(x0)",
        "sw x2, 4(x0)",
        "sw x3, 8(x0)"
    ]

    machine_code = assemble_program(program)
    
    imem_reset(dut, False)
    force_imem(dut, "big", BIG_IMEM_OFFSET, machine_code)

    await ClockCycles(dut.clk, 2)
    
    
    dmem_reset(dut, False)
    force_dmem(dut, 0, [0x1, 0x2, 0x3])
    await ClockCycles(dut.clk, 10)
    
    cpu_reset(dut, False)
    await ClockCycles(dut.clk, 40)
    
    dmem_value = read_dmem(dut, 0, 3)
    assert dmem_value == [2, 4, 6]


@cocotb.test(skip=True)
async def little_multiply(dut):
    """Test that the little core can handle a multiply instruction"""

    cocotb.start_soon(Clock(dut.clk, 1, units="ns").start())

    # Put cpus and memory in reset
    cpu_reset(dut, True)
    imem_reset(dut, True)
    dmem_reset(dut, True)
    init_dmem(dut)

    await RisingEdge(dut.clk)
    await ClockCycles(dut.clk, 1)

    program = [
        "addi x1, x0, 2",
        "addi x2, x0, 2",
        "mul x3, x2, x1",
        "sw x3, 0(x0)"
    ]

    machine_code = assemble_program(program)
    imem_reset(dut, False)
    force_imem(dut, "little", LITTLE_IMEM_OFFSET, machine_code)


    await ClockCycles(dut.clk, 2)
    
    
    dmem_reset(dut, False)
    await ClockCycles(dut.clk, 10)
    
    cpu_reset(dut, False)
    await ClockCycles(dut.clk, 41)
    
    cpu_reset(dut, True)
    dmem_value = read_dmem(dut, 0, 1)[0]
    assert dmem_value == 4


@cocotb.test(skip=True)
async def little_multiply_with_big_instructions(dut):
    """Test that the big core can be interrupted and resume"""

    cocotb.start_soon(Clock(dut.clk, 1, units="ns").start())

    # Put cpus and memory in reset
    cpu_reset(dut, True)
    imem_reset(dut, True)
    dmem_reset(dut, True)
    init_dmem(dut)

    await RisingEdge(dut.clk)
    await ClockCycles(dut.clk, 1)

    little_program = [
        "xor x0, x0, x0",
        "nop",
        "nop",
        "nop",
        "nop",
        "nop",
        "nop",
        "addi x5, x0, 2",
        "addi x6, x0, 2",
        "mul x7, x6, x5",
        "xor x0, x0, x0",
        "sw x7, 0(x0)",
        "HALT:",
        "jal HALT"
    ]

    big_program = [
        "addi x1, x0, 1"
    ]
    big_program.extend(["addi x1, x1, 1"] * 99)
    big_program.extend(["xor x0, x0, x0", "sw x1, 4(x0)"])
    big_program.extend(
        ["HALT:",
        "jal HALT"]
    )
    
    little_mc = assemble_program(little_program)
    big_mc = assemble_program(big_program)
    big_mc[-2] = 0x00102223
    imem_reset(dut, False)
    force_imem(dut, "little", LITTLE_IMEM_OFFSET, little_mc)
    force_imem(dut, "big", BIG_IMEM_OFFSET, big_mc)

    await ClockCycles(dut.clk, 2)
    
    dmem_reset(dut, False)
    await ClockCycles(dut.clk, 10)
    
    cpu_reset(dut, False)
    await ClockCycles(dut.clk, 130)
    
    cpu_reset(dut, True)
    resp = read_dmem(dut, 0, 32)

    little_value = resp[0]
    big_value = resp[1]

    assert little_value == 4
    assert big_value == 100

@cocotb.test(skip=True)
async def little_multiply2x(dut):
    """Test that the little core can handle multiple multiply instructions"""

    cocotb.start_soon(Clock(dut.clk, 1, units="ns").start())

    # Put cpus and memory in reset
    cpu_reset(dut, True)
    imem_reset(dut, True)
    dmem_reset(dut, True)
    init_dmem(dut)

    await RisingEdge(dut.clk)
    await ClockCycles(dut.clk, 1)

    program = [
        "addi x5, x0, 2",
        "addi x6, x0, 2",
        "mul x7, x6, x5",
        "mul x7, x7, x5",
        "sw x7, 0(x0)"
    ]

    machine_code = assemble_program(program)
    imem_reset(dut, False)
    force_imem(dut, "little", LITTLE_IMEM_OFFSET, machine_code)


    await ClockCycles(dut.clk, 2)
    
    
    dmem_reset(dut, False)
    await ClockCycles(dut.clk, 10)
    
    cpu_reset(dut, False)
    await ClockCycles(dut.clk, 60)
    
    dmem_value = read_dmem(dut, 0)[0]
    assert dmem_value == 8

@cocotb.test(skip=True)
async def little_write_dmem(dut):
    """Test that the little core can handle multiple multiply instructions"""

    cocotb.start_soon(Clock(dut.clk, 1, units="ns").start())

    # Put cpus and memory in reset
    cpu_reset(dut, True)
    imem_reset(dut, True)
    dmem_reset(dut, True)
    init_dmem(dut)

    await RisingEdge(dut.clk)
    await ClockCycles(dut.clk, 1)

    program = [
        "xor x0, x0, x0",  # 0
        "addi x1, x0, 0",  # 4
        "addi x2, x0, 64", # 8
        "loop:",
        "addi x1, x1, 4",  # C
        "sw x1, 0(x1)",    # 10
        "blt x1, x2, loop" # 14
    ]

    machine_code = assemble_program(program)
    imem_reset(dut, False)
    
    force_imem(dut, "little", LITTLE_IMEM_OFFSET, machine_code)


    await ClockCycles(dut.clk, 2)
    
    
    dmem_reset(dut, False)
    await ClockCycles(dut.clk, 10)
    
    cpu_reset(dut, False)
    await ClockCycles(dut.clk, 1000)
    
    cpu_reset(dut, True)
    dmem_value = read_dmem(dut, 0, 16)
    for data in zip(dmem_value, range(0, 64, 4)):
        assert data[0] == data[1]

@cocotb.test(skip=True)
async def big_write_dmem(dut):
    """Test that the little core can handle multiple multiply instructions"""

    cocotb.start_soon(Clock(dut.clk, 1, units="ns").start())

    # Put cpus and memory in reset
    cpu_reset(dut, True)
    imem_reset(dut, True)
    dmem_reset(dut, True)
    init_dmem(dut)

    await RisingEdge(dut.clk)
    await ClockCycles(dut.clk, 1)

    program = [
        "xor x0, x0, x0",  # 0
        "addi x1, x0, 0",  # 4
        "addi x2, x0, 64", # 8
        "loop:",
        "addi x1, x1, 4",  # C
        "sw x1, 0(x1)",    # 10
        "blt x1, x2, loop" # 14
    ]

    machine_code = assemble_program(program)
    imem_reset(dut, False)
    
    force_imem(dut, "big", BIG_IMEM_OFFSET, machine_code)


    await ClockCycles(dut.clk, 2)
    
    
    dmem_reset(dut, False)
    await ClockCycles(dut.clk, 10)
    
    cpu_reset(dut, False)
    await ClockCycles(dut.clk, 1000)
    
    cpu_reset(dut, True)
    dmem_value = read_dmem(dut, 0, 16)
    for data in zip(dmem_value, range(0, 64, 4)):
        assert data[0] == data[1]


@cocotb.test(skip=True)
async def big_sw_offset_test(dut):
    """Test that the little core can handle multiple multiply instructions"""

    cocotb.start_soon(Clock(dut.clk, 1, units="ns").start())

    # Put cpus and memory in reset
    cpu_reset(dut, True)
    imem_reset(dut, True)
    dmem_reset(dut, True)
    init_dmem(dut)

    await RisingEdge(dut.clk)
    await ClockCycles(dut.clk, 1)

    program = [
        "xor x0, x0, x0",  # 0
    ]

    for i in range(16):
        lines = [f"addi x1, x0, {i*4}"]
        lines.append(f"sw x1, {i*4}(x0)")
        program.extend(lines)

    program.extend(
        ["HALT:",
        "jal HALT"]
    )

    machine_code = assemble_program(program)
    imem_reset(dut, False)
    
    force_imem(dut, "big", BIG_IMEM_OFFSET, machine_code)


    await ClockCycles(dut.clk, 2)
    
    
    dmem_reset(dut, False)
    await ClockCycles(dut.clk, 10)
    
    cpu_reset(dut, False)
    await ClockCycles(dut.clk, 1000)
    
    dmem_value = read_dmem(dut, 0, 16)
    for data in zip(dmem_value, range(0, 64, 4)):
        assert data[0] == data[1]


@cocotb.test(skip=True)
async def little_sw_offset_test(dut):
    """Test that the little core can handle multiple multiply instructions"""

    cocotb.start_soon(Clock(dut.clk, 1, units="ns").start())

    # Put cpus and memory in reset
    cpu_reset(dut, True)
    imem_reset(dut, True)
    dmem_reset(dut, True)
    init_dmem(dut)

    await RisingEdge(dut.clk)
    await ClockCycles(dut.clk, 1)

    program = [
        "xor x0, x0, x0",  # 0
    ]

    for i in range(16):
        lines = [f"addi x1, x0, {i*4}"]
        lines.append(f"sw x1, {i*4}(x0)")
        program.extend(lines)

    program.extend(
        ["HALT:",
        "jal x0, HALT"]
    )
    
    machine_code = assemble_program(program)
    imem_reset(dut, False)
    
    force_imem(dut, "little", LITTLE_IMEM_OFFSET, machine_code)


    await ClockCycles(dut.clk, 2)
    
    
    cpu_reset(dut, False)
    await ClockCycles(dut.clk, 1000)
    BIG_IMEM_OFFSET
    dmem_value = read_dmem(dut, 0, 16)
    for data in zip(dmem_value, range(0, 64, 4)):
        assert data[0] == data[1]


@cocotb.test(skip=True)
async def little_sw_multiply(dut):
    """Measure software multiply latency and accuracy"""

    cocotb.start_soon(Clock(dut.clk, 1, units="ns").start())

    tests: List[mult_test] = []
    max_value = 0xFFFF_FFFF
    for i in range(32):
        operand_a = max_value
        operand_b = 0x1 << i
        tests.append(mult_test(operand_a, operand_b))
    
    tests.append(mult_test(max_value, max_value))
    tests.append(mult_test(0, 0))

    hardware_results = []
    software_results = []
    mailbox_results = []
    baseline_results = []

    for test in tests:
        # Put cpus and memory in reset
        cpu_reset(dut, True)
        imem_reset(dut, True)
        dmem_reset(dut, True)
        init_dmem(dut)
        
        await RisingEdge(dut.clk)
        await ClockCycles(dut.clk, 1)

        with open("test_programs/multiply_latency/hardware_multiply.S", "r") as f:
            little_program = f.readlines()

        little_mc = assemble_program(little_program)
        imem_reset(dut, False)
        force_imem(dut, "little", LITTLE_IMEM_OFFSET, little_mc)

        await ClockCycles(dut.clk, 2)
        
        dmem_reset(dut, False)
        force_dmem(dut, 0, [test.operand_a, test.operand_b])

        if test.expected == 0:
            force_dmem(dut, 2, [0xFFFFFFFF])

        await ClockCycles(dut.clk, 10)
        
        cpu_reset(dut, False)

        print(test)
        
        hardware_cycles = 0
        if test.expected == 0:
            hardware_value = 0xFFFFFFFF
        else:
            hardware_value = 0

        while hardware_value != test.expected:
            await ClockCycles(dut.clk, 1)
            hardware_value = read_dmem(dut, 2, 1)[0]
            hardware_cycles += 1
        
        hardware_results.append(hardware_cycles)

        cpu_reset(dut, True)
        imem_reset(dut, True)
        dmem_reset(dut, True)
        init_dmem(dut)

        with open("test_programs/multiply_latency/software_multiply.S", "r") as f:
            little_program = f.readlines()

        little_mc = assemble_program(little_program)
        imem_reset(dut, False)
        force_imem(dut, "little", LITTLE_IMEM_OFFSET, little_mc)

        await ClockCycles(dut.clk, 2)
        
        dmem_reset(dut, False)
        force_dmem(dut, 0, [test.operand_a, test.operand_b])

        if test.expected == 0:
            force_dmem(dut, 2, [0xFFFFFFFF])

        await ClockCycles(dut.clk, 10)
        
        cpu_reset(dut, False)

        software_cycles = 0
        if test.expected == 0:
            software_value = 0xFFFFFFFF
        else:
            software_value = 0

        while software_value != test.expected:
            await ClockCycles(dut.clk, 1)
            software_value = read_dmem(dut, 2, 1)[0]
            software_cycles += 1

        software_results.append(software_cycles)

        cpu_reset(dut, True)
        imem_reset(dut, True)
        dmem_reset(dut, True)
        init_dmem(dut)

        with open("test_programs/multiply_latency/little_mailbox.S", "r") as f:
            little_program = f.readlines()

        with open("test_programs/multiply_latency/big_mailbox.S", "r") as f:
            big_program = f.readlines()

        little_mc = assemble_program(little_program)
        big_mc = assemble_program(big_program)
        imem_reset(dut, False)
        force_imem(dut, "little", LITTLE_IMEM_OFFSET, little_mc)
        force_imem(dut, "big", BIG_IMEM_OFFSET, big_mc)

        await ClockCycles(dut.clk, 2)
        
        dmem_reset(dut, False)
        force_dmem(dut, 0, [test.operand_a, test.operand_b])

        if test.expected == 0:
            force_dmem(dut, 2, [0xFFFFFFFF])

        await ClockCycles(dut.clk, 10)
        
        cpu_reset(dut, False)

        mailbox_cycles = 0
        if test.expected == 0:
            software_value = 0xFFFFFFFF
        else:
            software_value = 0

        while software_value != test.expected:
            await ClockCycles(dut.clk, 1)
            software_value = read_dmem(dut, 2, 1)[0]
            mailbox_cycles += 1

        mailbox_results.append(mailbox_cycles)

        # Put cpus and memory in reset
        cpu_reset(dut, True)
        imem_reset(dut, True)
        dmem_reset(dut, True)
        init_dmem(dut)
        
        await RisingEdge(dut.clk)
        await ClockCycles(dut.clk, 1)

        with open("test_programs/multiply_latency/baseline.S", "r") as f:
            little_program = f.readlines()

        little_mc = assemble_program(little_program)
        imem_reset(dut, False)
        force_imem(dut, "little", LITTLE_IMEM_OFFSET, little_mc)

        await ClockCycles(dut.clk, 2)
        
        dmem_reset(dut, False)
        force_dmem(dut, 0, [test.operand_a, test.operand_b])

        if test.expected == 0:
            force_dmem(dut, 2, [0xFFFFFFFF])

        await ClockCycles(dut.clk, 10)
        
        cpu_reset(dut, False)
        
        baseline_cycles = 0
        if test.operand_a + test.operand_b == 0:
            baseline_value = 0xFFFFFFFF
        else:
            baseline_value = 0
        
        while baseline_value != (test.operand_a + test.operand_b) & 0xFFFF_FFFF:
            await ClockCycles(dut.clk, 1)
            baseline_value = read_dmem(dut, 2, 1)[0]
            baseline_cycles += 1
        
        baseline_results.append(baseline_cycles)

    d = {"op_a": [tst.operand_a for tst in tests], "op_b": [tst.operand_b for tst in tests], "hardware_cycles": hardware_results, "software_cycles": software_results, "mailbox_cycles": mailbox_results, "baseline": baseline_results}
    
    df = pd.DataFrame(data=d)

    df.to_csv(DATA_DIR + "hw_vs_sw_vs_mailbox_multiply.csv")

    assert True

@cocotb.test(skip=True)
async def loading_c(dut):
    """Measure software multiply latency and accuracy"""

    cocotb.start_soon(Clock(dut.clk, 1, units="ns").start())

    # Put cpus and memory in reset
    cpu_reset(dut, True)
    imem_reset(dut, True)
    dmem_reset(dut, True)
    init_dmem(dut)

    await RisingEdge(dut.clk)
    await ClockCycles(dut.clk, 1)

    machine_code = compile_c("../test_program.c")
    imem_reset(dut, False)
    force_imem(dut, "little", LITTLE_IMEM_OFFSET, machine_code)

    await ClockCycles(dut.clk, 2)
    
    dmem_reset(dut, False)
    await ClockCycles(dut.clk, 10)
    
    cpu_reset(dut, False)


    await ClockCycles(dut.clk, 100)

    cpu_reset(dut, True)
    dmem = read_dmem(dut, 0, 65535)

    for i, value in enumerate(dmem):
        if value.value > 0:
            print(f"{i}: {value.value}")

@cocotb.test(skip=True)
async def test_matrix_multiply(dut):
    """Measure software multiply latency and accuracy"""

    cocotb.start_soon(Clock(dut.clk, 1, units="ns").start())

    # Put cpus and memory in reset
    cpu_reset(dut, True)
    imem_reset(dut, True)
    dmem_reset(dut, True)
    init_dmem(dut)

    await RisingEdge(dut.clk)
    await ClockCycles(dut.clk, 1)

    with open("test_programs/matrix_multiply/3x3_matrix_multiply.S", "r") as f:
            little_program = f.readlines()
    
    machine_code = assemble_program(little_program)
    imem_reset(dut, False)
    force_imem(dut, "little", LITTLE_IMEM_OFFSET, machine_code)

    await ClockCycles(dut.clk, 2)
    
    force_dmem(dut, 0, [1, 2, 3, 1, 2, 3, 1, 2, 3])
    force_dmem(dut, 9, [1, 1, 1, 2, 2, 2, 3, 3, 3])
    force_dmem(dut, 18, [0, 0, 0, 0, 0, 0, 0, 0, 0])

    dmem_reset(dut, False)
    await ClockCycles(dut.clk, 10)
    
    cpu_reset(dut, False)

    end_value = read_dmem(dut, 27)[0]
    
    while end_value.value == 0:
        await ClockCycles(dut.clk, 1)
        end_value = read_dmem(dut, 27)[0]

    cpu_reset(dut, True)
    dmem = read_dmem(dut, 0, 65535)
    
    for i, value in enumerate(dmem):
        if value.value > 0:
            print(f"{i*4:02X}: {value.value}")


@cocotb.test(skip=True)
async def data_collecting_matrix_multiply(dut):
    """Measure software multiply latency and accuracy"""

    cocotb.start_soon(Clock(dut.clk, 1, units="ns").start())

    # Test 0 - hardware
    hardware_cycles = 0
    cpu_reset(dut, True)
    imem_reset(dut, True)
    dmem_reset(dut, True)
    init_dmem(dut)

    await RisingEdge(dut.clk)
    await ClockCycles(dut.clk, 1)

    with open("test_programs/matrix_multiply/3x3_matrix_multiply.S", "r") as f:
            little_program = f.readlines()

    little_mc = assemble_program(little_program)
    
    imem_reset(dut, False)
    force_imem(dut, "little", LITTLE_IMEM_OFFSET, little_mc)

    await ClockCycles(dut.clk, 2)
    
    force_dmem(dut, 0, [1, 2, 3, 1, 2, 3, 1, 2, 3])
    force_dmem(dut, 9, [1, 1, 1, 2, 2, 2, 3, 3, 3])
    force_dmem(dut, 18, [0, 0, 0, 0, 0, 0, 0, 0, 0])

    dmem_reset(dut, False)
    await ClockCycles(dut.clk, 10)
    
    cpu_reset(dut, False)

    end_value = read_dmem(dut, 27)[0]
    
    while end_value.value == 0:
        await ClockCycles(dut.clk, 1)
        end_value = read_dmem(dut, 27)[0]
        hardware_cycles += 1

    cpu_reset(dut, True)

    # Test 1 - mailbox
    mailbox_cycles = 0
    cpu_reset(dut, True)
    imem_reset(dut, True)
    dmem_reset(dut, True)
    init_dmem(dut)

    await RisingEdge(dut.clk)
    await ClockCycles(dut.clk, 1)

    with open("test_programs/matrix_multiply/3x3_matrix_multiply_mailbox.S", "r") as f:
            little_program = f.readlines()

    with open("test_programs/matrix_multiply/big_mailbox.S", "r") as f:
            big_program = f.readlines()
    
    little_mc = assemble_program(little_program)
    big_mc = assemble_program(big_program)
    
    imem_reset(dut, False)
    force_imem(dut, "little", LITTLE_IMEM_OFFSET, little_mc)
    force_imem(dut, "big", BIG_IMEM_OFFSET, big_mc)

    await ClockCycles(dut.clk, 2)
    
    force_dmem(dut, 0, [1, 2, 3, 1, 2, 3, 1, 2, 3])
    force_dmem(dut, 9, [1, 1, 1, 2, 2, 2, 3, 3, 3])
    force_dmem(dut, 18, [0, 0, 0, 0, 0, 0, 0, 0, 0])

    dmem_reset(dut, False)
    await ClockCycles(dut.clk, 10)
    
    cpu_reset(dut, False)

    end_value = read_dmem(dut, 27)[0]
    
    while end_value.value == 0:
        await ClockCycles(dut.clk, 1)
        end_value = read_dmem(dut, 27)[0]
        mailbox_cycles += 1

    cpu_reset(dut, True)

    # Test 2 - software 0's
    software_zero_cycles = 0
    cpu_reset(dut, True)
    imem_reset(dut, True)
    dmem_reset(dut, True)
    init_dmem(dut)

    await RisingEdge(dut.clk)
    await ClockCycles(dut.clk, 1)

    with open("test_programs/matrix_multiply/3x3_matrix_multiply_software.S", "r") as f:
            little_program = f.readlines()

    little_mc = assemble_program(little_program)
    
    imem_reset(dut, False)
    force_imem(dut, "little", LITTLE_IMEM_OFFSET, little_mc)

    await ClockCycles(dut.clk, 2)
    
    force_dmem(dut, 0, [0, 0, 0, 0, 0, 0, 0, 0, 0])
    force_dmem(dut, 9, [0, 0, 0, 0, 0, 0, 0, 0, 0])
    force_dmem(dut, 18, [0, 0, 0, 0, 0, 0, 0, 0, 0])

    dmem_reset(dut, False)
    await ClockCycles(dut.clk, 10)
    
    cpu_reset(dut, False)

    end_value = read_dmem(dut, 27)[0]
    
    while end_value.value == 0:
        await ClockCycles(dut.clk, 1)
        end_value = read_dmem(dut, 27)[0]
        software_zero_cycles += 1

    cpu_reset(dut, True)

    # Test 3 - software F's
    software_max_cycles = 0
    cpu_reset(dut, True)
    imem_reset(dut, True)
    dmem_reset(dut, True)
    init_dmem(dut)

    await RisingEdge(dut.clk)
    await ClockCycles(dut.clk, 1)

    with open("test_programs/matrix_multiply/3x3_matrix_multiply_software.S", "r") as f:
            little_program = f.readlines()
 
    little_mc = assemble_program(little_program)
    
    imem_reset(dut, False)
    force_imem(dut, "little", LITTLE_IMEM_OFFSET, little_mc)

    await ClockCycles(dut.clk, 2)
    
    force_dmem(dut, 0, [0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF])
    force_dmem(dut, 9, [0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF])
    force_dmem(dut, 18, [0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF])

    dmem_reset(dut, False)
    await ClockCycles(dut.clk, 10)
    
    cpu_reset(dut, False)

    end_value = read_dmem(dut, 27)[0]
    
    while end_value.value == 0:
        await ClockCycles(dut.clk, 1)
        end_value = read_dmem(dut, 27)[0]
        software_max_cycles += 1

    cpu_reset(dut, True)
    
    d = {"hardware_cycles": [hardware_cycles], "software_zero_cycles": [software_zero_cycles], "software_max_cycles": [software_max_cycles], "mailbox_cycles": [mailbox_cycles]}
    
    df = pd.DataFrame(data=d)

    df.to_csv(DATA_DIR + "matrix_multiply.csv")


@cocotb.test(skip=True)
async def data_collecting_matrix_multiply_vcd_hardware(dut):
    """Measure software multiply latency and accuracy"""

    cocotb.start_soon(Clock(dut.clk, 1, units="ns").start())

    # Test 0 - hardware
    cpu_reset(dut, True)
    imem_reset(dut, True)
    dmem_reset(dut, True)
    init_dmem(dut)

    await RisingEdge(dut.clk)
    await ClockCycles(dut.clk, 1)

    with open("test_programs/matrix_multiply/3x3_matrix_multiply.S", "r") as f:
            little_program = f.readlines()

    little_mc = assemble_program(little_program)
    
    imem_reset(dut, False)
    force_imem(dut, "little", LITTLE_IMEM_OFFSET, little_mc)

    await ClockCycles(dut.clk, 2)
    
    force_dmem(dut, 0, [1, 2, 3, 1, 2, 3, 1, 2, 3])
    force_dmem(dut, 9, [1, 1, 1, 2, 2, 2, 3, 3, 3])
    force_dmem(dut, 18, [0, 0, 0, 0, 0, 0, 0, 0, 0])

    dmem_reset(dut, False)
    await ClockCycles(dut.clk, 10)
    
    cpu_reset(dut, False)

    end_value = read_dmem(dut, 27)[0]
    
    while end_value.value == 0:
        await ClockCycles(dut.clk, 1)
        end_value = read_dmem(dut, 27)[0]


@cocotb.test(skip=True)
async def data_collecting_matrix_multiply_vcd_mailbox(dut):
    cocotb.start_soon(Clock(dut.clk, 1, units="ns").start())

    # Test 1 - mailbox
    cpu_reset(dut, True)
    imem_reset(dut, True)
    dmem_reset(dut, True)
    init_dmem(dut)

    await RisingEdge(dut.clk)
    await ClockCycles(dut.clk, 1)

    with open("test_programs/matrix_multiply/3x3_matrix_multiply_mailbox.S", "r") as f:
            little_program = f.readlines()

    with open("test_programs/matrix_multiply/big_mailbox.S", "r") as f:
            big_program = f.readlines()
    
    little_mc = assemble_program(little_program)
    big_mc = assemble_program(big_program)
    
    imem_reset(dut, False)
    force_imem(dut, "little", LITTLE_IMEM_OFFSET, little_mc)
    force_imem(dut, "big", BIG_IMEM_OFFSET, big_mc)

    await ClockCycles(dut.clk, 2)
    
    force_dmem(dut, 0, [1, 2, 3, 1, 2, 3, 1, 2, 3])
    force_dmem(dut, 9, [1, 1, 1, 2, 2, 2, 3, 3, 3])
    force_dmem(dut, 18, [0, 0, 0, 0, 0, 0, 0, 0, 0])

    dmem_reset(dut, False)
    await ClockCycles(dut.clk, 10)
    
    cpu_reset(dut, False)

    end_value = read_dmem(dut, 27)[0]
    
    while end_value.value == 0:
        await ClockCycles(dut.clk, 1)
        end_value = read_dmem(dut, 27)[0]

@cocotb.test(skip=True)
async def data_collecting_matrix_multiply_vcd_software_min(dut):
    cocotb.start_soon(Clock(dut.clk, 1, units="ns").start())
    # Test 2 - software 0's
    cpu_reset(dut, True)
    imem_reset(dut, True)
    dmem_reset(dut, True)
    init_dmem(dut)

    await RisingEdge(dut.clk)
    await ClockCycles(dut.clk, 1)

    with open("test_programs/matrix_multiply/3x3_matrix_multiply_software.S", "r") as f:
            little_program = f.readlines()

    little_mc = assemble_program(little_program)
    
    imem_reset(dut, False)
    force_imem(dut, "little", LITTLE_IMEM_OFFSET, little_mc)

    await ClockCycles(dut.clk, 2)
    
    force_dmem(dut, 0, [0, 0, 0, 0, 0, 0, 0, 0, 0])
    force_dmem(dut, 9, [0, 0, 0, 0, 0, 0, 0, 0, 0])
    force_dmem(dut, 18, [0, 0, 0, 0, 0, 0, 0, 0, 0])

    dmem_reset(dut, False)
    await ClockCycles(dut.clk, 10)
    
    cpu_reset(dut, False)

    end_value = read_dmem(dut, 27)[0]
    
    while end_value.value == 0:
        await ClockCycles(dut.clk, 1)
        end_value = read_dmem(dut, 27)[0]


@cocotb.test(skip=True)
async def data_collecting_matrix_multiply_vcd_software_max(dut):
    cocotb.start_soon(Clock(dut.clk, 1, units="ns").start())

    # Test 3 - software F's
    cpu_reset(dut, True)
    imem_reset(dut, True)
    dmem_reset(dut, True)
    init_dmem(dut)

    await RisingEdge(dut.clk)
    await ClockCycles(dut.clk, 1)

    with open("test_programs/matrix_multiply/3x3_matrix_multiply_software.S", "r") as f:
            little_program = f.readlines()
 
    little_mc = assemble_program(little_program)
    
    imem_reset(dut, False)
    force_imem(dut, "little", LITTLE_IMEM_OFFSET, little_mc)

    await ClockCycles(dut.clk, 2)
    
    force_dmem(dut, 0, [0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF])
    force_dmem(dut, 9, [0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF])
    force_dmem(dut, 18, [0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF])

    dmem_reset(dut, False)
    await ClockCycles(dut.clk, 10)
    
    cpu_reset(dut, False)

    end_value = read_dmem(dut, 27)[0]
    
    while end_value.value == 0:
        await ClockCycles(dut.clk, 1)
        end_value = read_dmem(dut, 27)[0]


@cocotb.test(skip=True)
async def test_hashing(dut):
    """Test that the hash code works as expected"""

    cocotb.start_soon(Clock(dut.clk, 1, units="ns").start())

    # Put cpus and memory in reset
    cpu_reset(dut, True)
    imem_reset(dut, True)
    dmem_reset(dut, True)
    init_dmem(dut)

    await RisingEdge(dut.clk)
    await ClockCycles(dut.clk, 1)

    with open("test_programs/hashing/adler32_mailbox.S", "r") as f:
            little_program = f.readlines()

    with open("test_programs/hashing/big_mailbox.S", "r") as f:
            big_program = f.readlines()
    
    little_mc = assemble_program(little_program)
    big_mc = assemble_program(big_program)
    imem_reset(dut, False)
    force_imem(dut, "little", LITTLE_IMEM_OFFSET, little_mc)
    force_imem(dut, "big", BIG_IMEM_OFFSET, big_mc)

    await ClockCycles(dut.clk, 2)
    
    force_dmem(dut, 0, [0])

    input_word = "Wikipedia"
    force_dmem(dut, 1, [len(input_word)])

    ascii_input = [ord(c) for c in input_word]

    force_dmem(dut, 2, ascii_input)

    dmem_reset(dut, False)
    await ClockCycles(dut.clk, 10)
    
    cpu_reset(dut, False)

    end_value = read_dmem(dut, 0)[0]
    
    while end_value.value == 0:
        await ClockCycles(dut.clk, 1)
        end_value = read_dmem(dut, 0)[0]

    cpu_reset(dut, True)
    
    print(f"End value: {end_value.value}")
    

@cocotb.test(skip=True)
async def data_collecting_hashing(dut):
    """Measure software multiply latency and accuracy"""

    cocotb.start_soon(Clock(dut.clk, 1, units="ns").start())

    np.random.seed(1)
    tests = []
    for i in range(1):
        for j in range(1, 12):
            string_list = list(np.random.choice(list(string.ascii_letters), size=j))
            tests.append("".join(string_list))

    hardware_results_8 = []
    software_results_8 = []
    mailbox_results_8 = []

    hardware_results_64k = []
    software_results_64k = []
    mailbox_results_64k = []

    cycle_step_size = 1

    for test in tests:
        initial_dmem = [0, len(test)]
        ascii_values = [ord(c) for c in test]
        initial_dmem.extend(ascii_values)

        # Table 8
        expected_result = calculate_adler32(test, 8)
        
        # Hardware
        cpu_reset(dut, True)
        imem_reset(dut, True)
        dmem_reset(dut, True)
        init_dmem(dut)
        
        await RisingEdge(dut.clk)
        await ClockCycles(dut.clk, 1)

        with open("test_programs/hashing/table_size_8/adler32.S", "r") as f:
            little_program = f.readlines()

        little_mc = assemble_program(little_program)
        imem_reset(dut, False)
        force_imem(dut, "little", LITTLE_IMEM_OFFSET, big_mc)

        await ClockCycles(dut.clk, 2)
        
        dmem_reset(dut, False)
        force_dmem(dut, 0, initial_dmem)

        await ClockCycles(dut.clk, 10)
        
        cpu_reset(dut, False)

        print(test)
        cycles = 0
        value = read_dmem(dut, 0, 1)[0]
        while value != expected_result:
            await ClockCycles(dut.clk, cycle_step_size)
            value = read_dmem(dut, 0, 1)[0]
            cycles += cycle_step_size
        
        hardware_results_8.append(cycles)


        # Software
        cpu_reset(dut, True)
        imem_reset(dut, True)
        dmem_reset(dut, True)
        init_dmem(dut)

        with open("test_programs/hashing/table_size_8/adler32_software.S", "r") as f:
            little_program = f.readlines()

        little_mc = assemble_program(little_program)
        imem_reset(dut, False)
        force_imem(dut, "little", LITTLE_IMEM_OFFSET, big_mc)

        await ClockCycles(dut.clk, 2)
        
        dmem_reset(dut, False)
        force_dmem(dut, 0, initial_dmem)

        await ClockCycles(dut.clk, 10)
        
        cpu_reset(dut, False)
        cycles = 0
        value = read_dmem(dut, 0, 1)[0]
        while value != expected_result:
            await ClockCycles(dut.clk, cycle_step_size)
            value = read_dmem(dut, 0, 1)[0]
            cycles += cycle_step_size

        software_results_8.append(cycles)

        # Mailbox
        cpu_reset(dut, True)
        imem_reset(dut, True)
        dmem_reset(dut, True)
        init_dmem(dut)

        with open("test_programs/hashing/table_size_8/adler32_mailbox.S", "r") as f:
            little_program = f.readlines()

        with open("test_programs/hashing/table_size_8/big_mailbox.S", "r") as f:
            big_program = f.readlines()

        little_mc = assemble_program(little_program)
        big_mc = assemble_program(big_program)
        imem_reset(dut, False)
        force_imem(dut, "little", LITTLE_IMEM_OFFSET, little_mc)
        force_imem(dut, "big", BIG_IMEM_OFFSET, big_mc)

        await ClockCycles(dut.clk, 2)
        
        dmem_reset(dut, False)
        force_dmem(dut, 0, initial_dmem)

        await ClockCycles(dut.clk, 10)
        
        cpu_reset(dut, False)
        cycles = 0
        value = read_dmem(dut, 0, 1)[0]
        while value != expected_result:
            await ClockCycles(dut.clk, cycle_step_size)
            value = read_dmem(dut, 0, 1)[0]
            cycles += cycle_step_size

        mailbox_results_8.append(cycles)

        # Table 64k
        expected_result = calculate_adler32(test, 65536)
        
        # Hardware
        cpu_reset(dut, True)
        imem_reset(dut, True)
        dmem_reset(dut, True)
        init_dmem(dut)
        
        await RisingEdge(dut.clk)
        await ClockCycles(dut.clk, 1)

        with open("test_programs/hashing/table_size_64k/adler32.S", "r") as f:
            little_program = f.readlines()

        little_mc = assemble_program(little_program)
        imem_reset(dut, False)
        force_imem(dut, "little", LITTLE_IMEM_OFFSET, big_mc)

        await ClockCycles(dut.clk, 2)
        
        dmem_reset(dut, False)
        force_dmem(dut, 0, initial_dmem)

        await ClockCycles(dut.clk, 10)
        
        cpu_reset(dut, False)

        cycles = 0
        value = read_dmem(dut, 0, 1)[0]
        while value != expected_result:
            await ClockCycles(dut.clk, cycle_step_size)
            value = read_dmem(dut, 0, 1)[0]
            cycles += cycle_step_size
        
        hardware_results_64k.append(cycles)


        # Software
        cpu_reset(dut, True)
        imem_reset(dut, True)
        dmem_reset(dut, True)
        init_dmem(dut)

        with open("test_programs/hashing/table_size_64k/adler32_software.S", "r") as f:
            little_program = f.readlines()

        little_mc = assemble_program(little_program)
        imem_reset(dut, False)
        force_imem(dut, "little", LITTLE_IMEM_OFFSET, big_mc)

        await ClockCycles(dut.clk, 2)
        
        dmem_reset(dut, False)
        force_dmem(dut, 0, initial_dmem)

        await ClockCycles(dut.clk, 10)
        
        cpu_reset(dut, False)
        cycles = 0
        value = read_dmem(dut, 0, 1)[0]
        while value != expected_result:
            await ClockCycles(dut.clk, cycle_step_size)
            value = read_dmem(dut, 0, 1)[0]
            cycles += cycle_step_size

        software_results_64k.append(cycles)

        # Mailbox
        cpu_reset(dut, True)
        imem_reset(dut, True)
        dmem_reset(dut, True)
        init_dmem(dut)

        with open("test_programs/hashing/table_size_64k/adler32_mailbox.S", "r") as f:
            little_program = f.readlines()

        with open("test_programs/hashing/table_size_64k/big_mailbox.S", "r") as f:
            big_program = f.readlines()

        little_mc = assemble_program(little_program)
        big_mc = assemble_program(big_program)
        imem_reset(dut, False)
        force_imem(dut, "little", LITTLE_IMEM_OFFSET, little_mc)
        force_imem(dut, "big", BIG_IMEM_OFFSET, big_mc)

        await ClockCycles(dut.clk, 2)
        
        dmem_reset(dut, False)
        force_dmem(dut, 0, initial_dmem)

        await ClockCycles(dut.clk, 10)
        
        cpu_reset(dut, False)
        cycles = 0
        value = read_dmem(dut, 0, 1)[0]
        while value != expected_result:
            await ClockCycles(dut.clk, cycle_step_size)
            value = read_dmem(dut, 0, 1)[0]
            cycles += cycle_step_size

        mailbox_results_64k.append(cycles)
    
    d = {"input": tests, 
         "length": [len(tst) for tst in tests], 
         "hardware_cycles_8": hardware_results_8, 
         "software_cycles_8": software_results_8, 
         "mailbox_cycles_8": mailbox_results_8,
         "hardware_cycles_64k": hardware_results_64k, 
         "software_cycles_64k": software_results_64k, 
         "mailbox_cycles_64k": mailbox_results_64k
        }
    
    df = pd.DataFrame(data=d)

    df.to_csv(DATA_DIR + "hw_vs_sw_vs_mailbox_hashing.csv")


@cocotb.test(skip=True)
async def data_collecting_hashing_min_max_inputs(dut):
    """Measure software multiply latency and accuracy"""

    cocotb.start_soon(Clock(dut.clk, 1, units="ns").start())

    np.random.seed(1)
    tests = []
    
    hardware_results_t8 = []
    software_min_results_t8 = []
    software_max_results_t8 = []
    mailbox_results_t8 = []

    hardware_results_t64k = []
    software_min_results_t64k = []
    software_max_results_t64k = []
    mailbox_results_t64k = []

    cycle_step_size = 1

    hardware_base_input = [127]
    mailbox_base_input = [127]
    software_max_base_input = [127]
    software_min_base_input = [0]

    for i in range(1, 12):
        print(i)
        tests.append(i)
        initial_dmem = [0, i]
        
        # Size 8
        # Hardware
        cpu_reset(dut, True)
        imem_reset(dut, True)
        dmem_reset(dut, True)
        init_dmem(dut)
        
        await RisingEdge(dut.clk)
        await ClockCycles(dut.clk, 1)

        with open("test_programs/hashing/table_size_8/adler32.S", "r") as f:
            little_program = f.readlines()

        little_mc = assemble_program(little_program)
        imem_reset(dut, False)
        force_imem(dut, "little", LITTLE_IMEM_OFFSET, big_mc)

        await ClockCycles(dut.clk, 2)
        
        dmem_reset(dut, False)
        expected_result = calculate_adler32_int(hardware_base_input * i, 8)
        force_dmem(dut, 0, initial_dmem + hardware_base_input * i)

        if expected_result == 0:
            force_dmem(dut, 0, [1])

        await ClockCycles(dut.clk, 10)
        
        cpu_reset(dut, False)
        
        cycles = 0
        value = read_dmem(dut, 0, 1)[0]
        
        while value != expected_result:
            await ClockCycles(dut.clk, cycle_step_size)
            value = read_dmem(dut, 0, 1)[0]
            cycles += cycle_step_size
        
        hardware_results_t8.append(cycles)


        # Software (min)
        cpu_reset(dut, True)
        imem_reset(dut, True)
        dmem_reset(dut, True)
        init_dmem(dut)

        with open("test_programs/hashing/table_size_8/adler32_software.S", "r") as f:
            little_program = f.readlines()

        little_mc = assemble_program(little_program)
        imem_reset(dut, False)
        force_imem(dut, "little", LITTLE_IMEM_OFFSET, big_mc)

        await ClockCycles(dut.clk, 2)
        
        dmem_reset(dut, False)
        expected_result = calculate_adler32_int(software_min_base_input * i, 8)
        force_dmem(dut, 0, initial_dmem + software_min_base_input * i)
        if expected_result == 0:
            force_dmem(dut, 0, [1])
        await ClockCycles(dut.clk, 10)
        
        cpu_reset(dut, False)
        cycles = 0
        value = read_dmem(dut, 0, 1)[0]
        
        while value != expected_result:
            await ClockCycles(dut.clk, cycle_step_size)
            value = read_dmem(dut, 0, 1)[0]
            cycles += cycle_step_size

        software_min_results_t8.append(cycles)

        # Software (max)
        cpu_reset(dut, True)
        imem_reset(dut, True)
        dmem_reset(dut, True)
        init_dmem(dut)

        with open("test_programs/hashing/table_size_8/adler32_software.S", "r") as f:
            little_program = f.readlines()

        little_mc = assemble_program(little_program)
        imem_reset(dut, False)
        force_imem(dut, "little", LITTLE_IMEM_OFFSET, big_mc)

        await ClockCycles(dut.clk, 2)
        
        dmem_reset(dut, False)
        expected_result = calculate_adler32_int(software_max_base_input * i, 8)
        force_dmem(dut, 0, initial_dmem + software_max_base_input * i)

        if expected_result == 0:
            force_dmem(dut, 0, [1])
        await ClockCycles(dut.clk, 10)
        
        cpu_reset(dut, False)
        cycles = 0
        value = read_dmem(dut, 0, 1)[0]
        
        while value != expected_result:
            await ClockCycles(dut.clk, cycle_step_size)
            value = read_dmem(dut, 0, 1)[0]
            cycles += cycle_step_size

        software_max_results_t8.append(cycles)

        # Mailbox
        cpu_reset(dut, True)
        imem_reset(dut, True)
        dmem_reset(dut, True)
        init_dmem(dut)

        with open("test_programs/hashing/table_size_8/adler32_mailbox.S", "r") as f:
            little_program = f.readlines()

        with open("test_programs/hashing/table_size_8/big_mailbox.S", "r") as f:
            big_program = f.readlines()

        little_mc = assemble_program(little_program)
        big_mc = assemble_program(big_program)
        imem_reset(dut, False)
        force_imem(dut, "little", LITTLE_IMEM_OFFSET, little_mc)
        force_imem(dut, "big", BIG_IMEM_OFFSET, big_mc)

        await ClockCycles(dut.clk, 2)
        
        dmem_reset(dut, False)
        expected_result = calculate_adler32_int(mailbox_base_input * i, 8)
        force_dmem(dut, 0, initial_dmem + mailbox_base_input * i)

        if expected_result == 0:
            force_dmem(dut, 0, [1])
        await ClockCycles(dut.clk, 10)
        
        cpu_reset(dut, False)
        cycles = 0
        value = read_dmem(dut, 0, 1)[0]
        
        while value != expected_result:
            await ClockCycles(dut.clk, cycle_step_size)
            value = read_dmem(dut, 0, 1)[0]
            cycles += cycle_step_size

        mailbox_results_t8.append(cycles)

        # Size 64k
        # Hardware
        cpu_reset(dut, True)
        imem_reset(dut, True)
        dmem_reset(dut, True)
        init_dmem(dut)
        
        await RisingEdge(dut.clk)
        await ClockCycles(dut.clk, 1)

        with open("test_programs/hashing/table_size_64k/adler32.S", "r") as f:
            little_program = f.readlines()

        little_mc = assemble_program(little_program)
        imem_reset(dut, False)
        force_imem(dut, "little", LITTLE_IMEM_OFFSET, big_mc)

        await ClockCycles(dut.clk, 2)
        
        dmem_reset(dut, False)
        force_dmem(dut, 0, initial_dmem + hardware_base_input * i)

        expected_result = calculate_adler32_int(hardware_base_input * i, 65536)
        if expected_result == 0:
            force_dmem(dut, 0, [1])

        await ClockCycles(dut.clk, 10)
        
        cpu_reset(dut, False)
        
        cycles = 0
        value = read_dmem(dut, 0, 1)[0]
        
        while value != expected_result:
            await ClockCycles(dut.clk, cycle_step_size)
            value = read_dmem(dut, 0, 1)[0]
            cycles += cycle_step_size
        
        hardware_results_t64k.append(cycles)


        # Software (min)
        cpu_reset(dut, True)
        imem_reset(dut, True)
        dmem_reset(dut, True)
        init_dmem(dut)

        with open("test_programs/hashing/table_size_64k/adler32_software.S", "r") as f:
            little_program = f.readlines()

        little_mc = assemble_program(little_program)
        imem_reset(dut, False)
        force_imem(dut, "little", LITTLE_IMEM_OFFSET, big_mc)

        await ClockCycles(dut.clk, 2)
        
        dmem_reset(dut, False)
        force_dmem(dut, 0, initial_dmem + software_min_base_input * i)

        expected_result = calculate_adler32_int(software_min_base_input * i, 65536)
        if expected_result == 0:
            force_dmem(dut, 0, [1])

        await ClockCycles(dut.clk, 10)
        
        cpu_reset(dut, False)
        cycles = 0
        value = read_dmem(dut, 0, 1)[0]
        
        while value != expected_result:
            await ClockCycles(dut.clk, cycle_step_size)
            value = read_dmem(dut, 0, 1)[0]
            cycles += cycle_step_size

        software_min_results_t64k.append(cycles)

        # Software (max)
        cpu_reset(dut, True)
        imem_reset(dut, True)
        dmem_reset(dut, True)
        init_dmem(dut)

        with open("test_programs/hashing/table_size_64k/adler32_software.S", "r") as f:
            little_program = f.readlines()

        little_mc = assemble_program(little_program)
        imem_reset(dut, False)
        force_imem(dut, "little", LITTLE_IMEM_OFFSET, big_mc)

        await ClockCycles(dut.clk, 2)
        
        dmem_reset(dut, False)
        force_dmem(dut, 0, initial_dmem + software_max_base_input * i)

        expected_result = calculate_adler32_int(software_max_base_input * i, 65536)
        if expected_result == 0:
            force_dmem(dut, 0, [1])

        await ClockCycles(dut.clk, 10)
        
        cpu_reset(dut, False)
        cycles = 0
        value = read_dmem(dut, 0, 1)[0]
        
        while value != expected_result:
            await ClockCycles(dut.clk, cycle_step_size)
            value = read_dmem(dut, 0, 1)[0]
            cycles += cycle_step_size

        software_max_results_t64k.append(cycles)

        # Mailbox
        cpu_reset(dut, True)
        imem_reset(dut, True)
        dmem_reset(dut, True)
        init_dmem(dut)

        with open("test_programs/hashing/table_size_64k/adler32_mailbox.S", "r") as f:
            little_program = f.readlines()

        with open("test_programs/hashing/table_size_64k/big_mailbox.S", "r") as f:
            big_program = f.readlines()

        little_mc = assemble_program(little_program)
        big_mc = assemble_program(big_program)
        imem_reset(dut, False)
        force_imem(dut, "little", LITTLE_IMEM_OFFSET, little_mc)
        force_imem(dut, "big", BIG_IMEM_OFFSET, big_mc)

        await ClockCycles(dut.clk, 2)
        
        dmem_reset(dut, False)
        force_dmem(dut, 0, initial_dmem + mailbox_base_input * i)

        expected_result = calculate_adler32_int(mailbox_base_input * i, 65536)
        if expected_result == 0:
            force_dmem(dut, 0, [1])
            
        await ClockCycles(dut.clk, 10)
        
        cpu_reset(dut, False)
        cycles = 0
        value = read_dmem(dut, 0, 1)[0]
        
        while value != expected_result:
            await ClockCycles(dut.clk, cycle_step_size)
            value = read_dmem(dut, 0, 1)[0]
            cycles += cycle_step_size

        mailbox_results_t64k.append(cycles)
    
    d = {"length": tests,
         "hardware_cycles_t8": hardware_results_t8, 
         "software_min_cycles_t8": software_min_results_t8, 
         "software_max_cycles_t8": software_max_results_t8, 
         "mailbox_cycles_t8": mailbox_results_t8,
         "hardware_cycles_t64k": hardware_results_t64k, 
         "software_min_cycles_t64k": software_min_results_t64k, 
         "software_max_cycles_t64k": software_max_results_t64k, 
         "mailbox_cycles_t64k": mailbox_results_t64k,
        }
    
    df = pd.DataFrame(data=d)

    df.to_csv(DATA_DIR + "hw_vs_sw_min_vs_sw_max_vs_mailbox_hashing.csv")




@cocotb.test(skip=False)
async def little_multiply_power_vcd_gen_hardware(dut):
    """Measure software multiply latency and accuracy"""

    cocotb.start_soon(Clock(dut.clk, 1, units="ns").start())
    max_value = 0xFFFF_FFFF
    test = mult_test(max_value, max_value)

    # Put cpus and memory in reset
    VCD_LOOPS = 1

    with open("test_programs/multiply_latency/hardware_multiply.S", "r") as f:
        little_program = f.readlines()

    little_mc = assemble_program(little_program)

    for i in range(VCD_LOOPS):
        print(i)
        cpu_reset(dut, True)
        imem_reset(dut, True)
        dmem_reset(dut, True)
        init_dmem(dut)
        
        await RisingEdge(dut.clk)
        await ClockCycles(dut.clk, 1)

        imem_reset(dut, False)
        force_imem(dut, "little", LITTLE_IMEM_OFFSET, little_mc)

        await ClockCycles(dut.clk, 2)
        
        dmem_reset(dut, False)
        force_dmem(dut, 0, [test.operand_a, test.operand_b])

        if test.expected == 0:
            force_dmem(dut, 2, [0xFFFFFFFF])

        await ClockCycles(dut.clk, 10)
        
        cpu_reset(dut, False)
        
        if test.expected == 0:
            hardware_value = 0xFFFFFFFF
        else:
            hardware_value = 0

        while hardware_value != test.expected:
            await ClockCycles(dut.clk, 1)
            hardware_value = read_dmem(dut, 2, 1)[0]

        
@cocotb.test(skip=False)
async def little_multiply_power_vcd_gen_software_min(dut):
    """Measure software multiply latency and accuracy"""

    cocotb.start_soon(Clock(dut.clk, 1, units="ns").start())
    max_value = 0x0000_0000
    test = mult_test(max_value, max_value)
    cpu_reset(dut, True)
    imem_reset(dut, True)
    dmem_reset(dut, True)
    init_dmem(dut)

    with open("test_programs/multiply_latency/software_multiply.S", "r") as f:
        little_program = f.readlines()

    little_mc = assemble_program(little_program)
    imem_reset(dut, False)
    force_imem(dut, "little", LITTLE_IMEM_OFFSET, little_mc)

    await ClockCycles(dut.clk, 2)
    
    dmem_reset(dut, False)
    force_dmem(dut, 0, [test.operand_a, test.operand_b])

    if test.expected == 0:
        force_dmem(dut, 2, [0xFFFFFFFF])

    await ClockCycles(dut.clk, 10)
    
    cpu_reset(dut, False)

    software_cycles = 0
    if test.expected == 0:
        software_value = 0xFFFFFFFF
    else:
        software_value = 0

    while software_value != test.expected:
        await ClockCycles(dut.clk, 1)
        software_value = read_dmem(dut, 2, 1)[0]
        software_cycles += 1

    assert True

@cocotb.test(skip=False)
async def little_multiply_power_vcd_gen_software_max(dut):
    """Measure software multiply latency and accuracy"""

    cocotb.start_soon(Clock(dut.clk, 1, units="ns").start())
    max_value = 0xFFFF_FFFF
    test = mult_test(max_value, max_value)
    cpu_reset(dut, True)
    imem_reset(dut, True)
    dmem_reset(dut, True)
    init_dmem(dut)

    with open("test_programs/multiply_latency/software_multiply.S", "r") as f:
        little_program = f.readlines()

    little_mc = assemble_program(little_program)
    imem_reset(dut, False)
    force_imem(dut, "little", LITTLE_IMEM_OFFSET, little_mc)

    await ClockCycles(dut.clk, 2)
    
    dmem_reset(dut, False)
    force_dmem(dut, 0, [test.operand_a, test.operand_b])

    if test.expected == 0:
        force_dmem(dut, 2, [0xFFFFFFFF])

    await ClockCycles(dut.clk, 10)
    
    cpu_reset(dut, False)

    software_cycles = 0
    if test.expected == 0:
        software_value = 0xFFFFFFFF
    else:
        software_value = 0

    while software_value != test.expected:
        await ClockCycles(dut.clk, 1)
        software_value = read_dmem(dut, 2, 1)[0]
        software_cycles += 1

    assert True

@cocotb.test(skip=False)
async def little_multiply_power_vcd_gen_mailbox(dut):
    """Measure software multiply latency and accuracy"""

    cocotb.start_soon(Clock(dut.clk, 1, units="ns").start())
    max_value = 0xFFFF_FFFF
    test = mult_test(max_value, max_value)
    cpu_reset(dut, True)
    imem_reset(dut, True)
    dmem_reset(dut, True)
    init_dmem(dut)

    with open("test_programs/multiply_latency/little_mailbox.S", "r") as f:
        little_program = f.readlines()

    with open("test_programs/multiply_latency/big_mailbox.S", "r") as f:
        big_program = f.readlines()

    little_mc = assemble_program(little_program)
    big_mc = assemble_program(big_program)
    imem_reset(dut, False)
    force_imem(dut, "little", LITTLE_IMEM_OFFSET, little_mc)
    force_imem(dut, "big", BIG_IMEM_OFFSET, big_mc)

    await ClockCycles(dut.clk, 2)
    
    dmem_reset(dut, False)
    force_dmem(dut, 0, [test.operand_a, test.operand_b])

    if test.expected == 0:
        force_dmem(dut, 2, [0xFFFFFFFF])

    await ClockCycles(dut.clk, 10)
    
    cpu_reset(dut, False)

    mailbox_cycles = 0
    if test.expected == 0:
        software_value = 0xFFFFFFFF
    else:
        software_value = 0

    while software_value != test.expected:
        await ClockCycles(dut.clk, 1)
        software_value = read_dmem(dut, 2, 1)[0]
        mailbox_cycles += 1

    assert True

@cocotb.test(skip=False)
async def little_multiply_power_vcd_gen_baseline(dut):
    """Measure software multiply latency and accuracy"""

    cocotb.start_soon(Clock(dut.clk, 1, units="ns").start())
    max_value = 0xFFFF_FFFF
    test = mult_test(max_value, max_value)
    # Put cpus and memory in reset
    cpu_reset(dut, True)
    imem_reset(dut, True)
    dmem_reset(dut, True)
    init_dmem(dut)
    
    await RisingEdge(dut.clk)
    await ClockCycles(dut.clk, 1)

    with open("test_programs/multiply_latency/baseline.S", "r") as f:
        little_program = f.readlines()

    little_mc = assemble_program(little_program)
    imem_reset(dut, False)
    force_imem(dut, "little", LITTLE_IMEM_OFFSET, little_mc)

    await ClockCycles(dut.clk, 2)
    
    dmem_reset(dut, False)
    force_dmem(dut, 0, [test.operand_a, test.operand_b])

    if test.expected == 0:
        force_dmem(dut, 2, [0xFFFFFFFF])

    await ClockCycles(dut.clk, 10)
    
    cpu_reset(dut, False)
    
    baseline_cycles = 0
    if test.operand_a + test.operand_b == 0:
        baseline_value = 0xFFFFFFFF
    else:
        baseline_value = 0
    
    while baseline_value != (test.operand_a + test.operand_b) & 0xFFFF_FFFF:
        await ClockCycles(dut.clk, 1)
        baseline_value = read_dmem(dut, 2, 1)[0]
        baseline_cycles += 1

    assert True




@cocotb.test(skip=True)
async def data_collecting_hashing_hardware(dut):
    """Measure software multiply latency and accuracy"""

    cocotb.start_soon(Clock(dut.clk, 1, units="ns").start())

    np.random.seed(1)

    cycle_step_size = 1

    hardware_base_input = [127]

    i = int(os.environ['HASH_INPUT_LENGTH'])
    size = int(os.environ['HASH_TABLE_SIZE'])

    if size == 8:
        directory = "table_size_8"
        mod_adler = 8
    else:
        directory = "table_size_64k"
        mod_adler = 65536
    
    initial_dmem = [0, i]

    cpu_reset(dut, True)
    imem_reset(dut, True)
    dmem_reset(dut, True)
    init_dmem(dut)
    
    await RisingEdge(dut.clk)
    await ClockCycles(dut.clk, 1)

    with open(f"test_programs/hashing/{directory}/adler32.S", "r") as f:
        little_program = f.readlines()

    little_mc = assemble_program(little_program)
    imem_reset(dut, False)
    force_imem(dut, "little", LITTLE_IMEM_OFFSET, little_mc)

    await ClockCycles(dut.clk, 2)
    
    dmem_reset(dut, False)
    expected_result = calculate_adler32_int(hardware_base_input * i, mod_adler)
    force_dmem(dut, 0, initial_dmem + hardware_base_input * i)

    if expected_result == 0:
        force_dmem(dut, 0, [1])

    await ClockCycles(dut.clk, 10)
    
    cpu_reset(dut, False)

    value = read_dmem(dut, 0, 1)[0]
    
    while value != expected_result:
        await ClockCycles(dut.clk, cycle_step_size)
        value = read_dmem(dut, 0, 1)[0]

    print(f"i={i}")
    print(f"size={size}")


@cocotb.test(skip=True)
async def data_collecting_hashing_software_min(dut):
    """Measure software multiply latency and accuracy"""

    cocotb.start_soon(Clock(dut.clk, 1, units="ns").start())

    np.random.seed(1)

    cycle_step_size = 1

    software_min_base_input = [0]

    i = int(os.environ['HASH_INPUT_LENGTH'])
    size = os.environ['HASH_TABLE_SIZE']

    if size == 8:
        directory = "table_size_8"
        mod_adler = 8
    else:
        directory = "table_size_64k"
        mod_adler = 65536
    
    initial_dmem = [0, i]

    # Software (min)
    cpu_reset(dut, True)
    imem_reset(dut, True)
    dmem_reset(dut, True)
    init_dmem(dut)

    with open(f"test_programs/hashing/{directory}/adler32_software.S", "r") as f:
        little_program = f.readlines()

    little_mc = assemble_program(little_program)
    imem_reset(dut, False)
    force_imem(dut, "little", LITTLE_IMEM_OFFSET, little_mc)

    await ClockCycles(dut.clk, 2)
    
    dmem_reset(dut, False)
    expected_result = calculate_adler32_int(software_min_base_input * i, mod_adler)
    force_dmem(dut, 0, initial_dmem + software_min_base_input * i)
    if expected_result == 0:
        force_dmem(dut, 0, [1])
    await ClockCycles(dut.clk, 10)
    
    cpu_reset(dut, False)
    value = read_dmem(dut, 0, 1)[0]
    
    while value != expected_result:
        await ClockCycles(dut.clk, cycle_step_size)
        value = read_dmem(dut, 0, 1)[0]


@cocotb.test(skip=True)
async def data_collecting_hashing_software_max(dut):
    """Measure software multiply latency and accuracy"""

    cocotb.start_soon(Clock(dut.clk, 1, units="ns").start())

    np.random.seed(1)

    cycle_step_size = 1

    software_max_base_input = [127]

    i = int(os.environ['HASH_INPUT_LENGTH'])
    size = os.environ['HASH_TABLE_SIZE']

    if size == 8:
        directory = "table_size_8"
        mod_adler = 8
    else:
        directory = "table_size_64k"
        mod_adler = 65536

    initial_dmem = [0, i]
    # Software (max)
    cpu_reset(dut, True)
    imem_reset(dut, True)
    dmem_reset(dut, True)
    init_dmem(dut)

    with open(f"test_programs/hashing/{directory}/adler32_software.S", "r") as f:
        little_program = f.readlines()

    little_mc = assemble_program(little_program)
    imem_reset(dut, False)
    force_imem(dut, "little", LITTLE_IMEM_OFFSET, little_mc)

    await ClockCycles(dut.clk, 2)
    
    dmem_reset(dut, False)
    expected_result = calculate_adler32_int(software_max_base_input * i, mod_adler)
    force_dmem(dut, 0, initial_dmem + software_max_base_input * i)

    if expected_result == 0:
        force_dmem(dut, 0, [1])
    await ClockCycles(dut.clk, 10)
    
    cpu_reset(dut, False)
    value = read_dmem(dut, 0, 1)[0]
    
    while value != expected_result:
        await ClockCycles(dut.clk, cycle_step_size)
        value = read_dmem(dut, 0, 1)[0]


@cocotb.test(skip=True)
async def data_collecting_hashing_mailbox(dut):
    """Measure software multiply latency and accuracy"""

    cocotb.start_soon(Clock(dut.clk, 1, units="ns").start())

    np.random.seed(1)

    cycle_step_size = 1

    mailbox_base_input = [127]

    i = int(os.environ['HASH_INPUT_LENGTH'])
    size = os.environ['HASH_TABLE_SIZE']

    if size == 8:
        directory = "table_size_8"
        mod_adler = 8
    else:
        directory = "table_size_64k"
        mod_adler = 65536

    initial_dmem = [0, i]

    # Mailbox
    cpu_reset(dut, True)
    imem_reset(dut, True)
    dmem_reset(dut, True)
    init_dmem(dut)

    with open(f"test_programs/hashing/{directory}/adler32_mailbox.S", "r") as f:
        little_program = f.readlines()

    with open(f"test_programs/hashing/{directory}/big_mailbox.S", "r") as f:
        big_program = f.readlines()

    little_mc = assemble_program(little_program)
    big_mc = assemble_program(big_program)
    imem_reset(dut, False)
    force_imem(dut, "little", LITTLE_IMEM_OFFSET, little_mc)
    force_imem(dut, "big", BIG_IMEM_OFFSET, big_mc)

    await ClockCycles(dut.clk, 2)
    
    dmem_reset(dut, False)
    expected_result = calculate_adler32_int(mailbox_base_input * i, mod_adler)
    force_dmem(dut, 0, initial_dmem + mailbox_base_input * i)

    if expected_result == 0:
        force_dmem(dut, 0, [1])
    await ClockCycles(dut.clk, 10)
    
    cpu_reset(dut, False)
    value = read_dmem(dut, 0, 1)[0]
    
    while value != expected_result:
        await ClockCycles(dut.clk, cycle_step_size)
        value = read_dmem(dut, 0, 1)[0]