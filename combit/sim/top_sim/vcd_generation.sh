# make TESTCASE=little_multiply_power_vcd_gen_hardware
# mv dump.vcd power_vcd/multiply_hardware.vcd
# make TESTCASE=little_multiply_power_vcd_gen_software_min
# mv dump.vcd power_vcd/multiply_software_min.vcd
# make TESTCASE=little_multiply_power_vcd_gen_software_max
# mv dump.vcd power_vcd/multiply_software_max.vcd
# make TESTCASE=little_multiply_power_vcd_gen_mailbox
# mv dump.vcd power_vcd/multiply_mailbox.vcd

# make TESTCASE=data_collecting_matrix_multiply_vcd_hardware
# mv dump.vcd power_vcd/matrix_multiply_hardware.vcd
# make TESTCASE=data_collecting_matrix_multiply_vcd_mailbox
# mv dump.vcd power_vcd/matrix_multiply_mailbox.vcd
# make TESTCASE=data_collecting_matrix_multiply_vcd_software_min
# mv dump.vcd power_vcd/matrix_multiply_software_min.vcd
# make TESTCASE=data_collecting_matrix_multiply_vcd_software_max
# mv dump.vcd power_vcd/matrix_multiply_software_max.vcd

for table_size in 8 64
do
    export HASH_TABLE_SIZE=$table_size
    for i in $(seq 12)
    do 
        export HASH_INPUT_LENGTH=$i
        make TESTCASE=data_collecting_hashing_hardware
        mv dump.vcd power_vcd/hash_hardware_${table_size}_${i}.vcd
        make TESTCASE=data_collecting_hashing_software_min
        mv dump.vcd power_vcd/hash_software_min_${table_size}_${i}.vcd
        make TESTCASE=data_collecting_hashing_software_max
        mv dump.vcd power_vcd/hash_software_max_${table_size}_${i}.vcd
        make TESTCASE=data_collecting_hashing_mailbox
        mv dump.vcd power_vcd/hash_mailbox_${table_size}_${i}.vcd
    done
done