from glob import glob
import os
import shutil
import subprocess
from time import sleep
import pyautogui
from pathlib import Path

files_to_parse = glob("multiply*.vcd")
for vcd in files_to_parse:
    print(vcd)
    # Open gtkwave manually
    process = subprocess.Popen(["gtkwave", vcd], stdout=subprocess.DEVNULL)
    sleep(3)
    locations = Path("screen_locs/")

    try:
        loc = pyautogui.locateOnScreen("screen_locs/signals.png", confidence=0.8)
    except pyautogui.ImageNotFoundException:
        loc = pyautogui.locateOnScreen("screen_locs/signals_blue.png", confidence=0.8)

    pyautogui.click(loc)
    pyautogui.rightClick(loc)

    pyautogui.moveRel(50, 25)
    pyautogui.moveRel(150, 0)
    sleep(0.1)
    pyautogui.moveRel(75, 25)
    pyautogui.click()

    pyautogui.press("Enter")
    sleep(5)
    loc = pyautogui.locateOnScreen("screen_locs/menu.png", confidence=0.8)
    pyautogui.click(loc, duration=0.2)
    pyautogui.press("Down")
    pyautogui.press("Enter")
    for i in range(3):
        pyautogui.press("Down")
    pyautogui.press("Enter")
    pyautogui.press("Enter")

    filename = f"parsed_{vcd}"
    pyautogui.moveRel(50, 0)
    pyautogui.click()
    pyautogui.typewrite(filename)

    pyautogui.moveRel(750, -40)
    sleep(1)
    pyautogui.click()
    pyautogui.click()
    sleep(5)
    try:
        shutil.move(filename, "../parsed_power_vcd")
    except shutil.Error:
        os.remove(f"../parsed_power_vcd/{filename}")
        shutil.move(filename, "../parsed_power_vcd")
    process.kill()

    