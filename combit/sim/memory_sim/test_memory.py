from random import randrange, sample
import cocotb
from cocotb.triggers import Timer, FallingEdge, ClockCycles, RisingEdge
from cocotb.clock import Clock
from cocotb.regression import TestFactory
import pytest
from cocotb.handle import Force

async def write_data(dut, port: int, addr: int, data: int ):

    setattr(dut, f"i_addr{port}", addr)
    setattr(dut, f"i_data{port}", data)
    setattr(dut, f"i_stb{port}", 1)
    setattr(dut, f"i_cyc{port}", 1)
    setattr(dut, f"i_we{port}", 1)
    await Timer(1, units="ns")

async def read_data(dut, port: int, addr: int)->int:

    setattr(dut, f"i_addr{port}", addr)
    setattr(dut, f"i_stb{port}", 1)
    setattr(dut, f"i_cyc{port}", 1)
    setattr(dut, f"i_we{port}", 0)

    await Timer(1, units="ns")

    return int(getattr(dut, f"o_data{port}").value)

async def incrementing(dut, port):
    """Write incrementing values to the entire memory range and read them back"""

    cocotb.start_soon(Clock(dut.clk, 1, units="ns").start())

    dut.rst.value = 0

    await Timer(5, units="ns")
    await FallingEdge(dut.clk)
    
    for i in range(0, 65535//4, 4):
        await write_data(dut, port, i, i)
        resp = await read_data(dut, port, i)
        
        assert resp == i

@cocotb.test(skip=True)
async def write_port0_read_port1(dut):
    """Write incrementing values to the entire memory range and read them back"""

    cocotb.start_soon(Clock(dut.clk, 1, units="ns").start())

    dut.rst.value = 0

    await Timer(5, units="ns")
    await FallingEdge(dut.clk)
    
    addr = 0
    data = 0xDEADBEEF
    await write_data(dut, 0, addr, data)
    resp = await read_data(dut, 1, addr)
    
    assert resp == data

async def zeros(dut, port):
    """Write incrementing values to the entire memory range and read them back"""

    cocotb.start_soon(Clock(dut.clk, 1, units="ns").start())

    dut.rst.value = 0

    await Timer(5, units="ns")
    await FallingEdge(dut.clk)
    
    data = 0
    for i in range(65535):
        await write_data(dut, port, i, data)
        resp = await read_data(dut, port, i)
        
        assert resp == data

async def ones(dut, port):
    """Write incrementing values to the entire memory range and read them back"""

    cocotb.start_soon(Clock(dut.clk, 1, units="ns").start())

    dut.rst.value = 0

    await Timer(5, units="ns")
    await FallingEdge(dut.clk)
    
    data = 0xFFFFFFFF
    for i in range(65535):
        await write_data(dut, port, i, data)
        resp = await read_data(dut, port, i)
        assert resp == data

async def random(dut, port):
    """Write incrementing values to the entire memory range and read them back"""

    cocotb.start_soon(Clock(dut.clk, 1, units="ns").start())

    dut.rst.value = 0

    await Timer(5, units="ns")
    await FallingEdge(dut.clk)
    
    for i in range(65535):
        data = randrange(0, 0xFFFFFFFF)
        await write_data(dut, port, i, data)
        resp = await read_data(dut, port, i)
        
        assert resp == data

@cocotb.test(skip=True)
async def test_force_write(dut):
    cocotb.start_soon(Clock(dut.clk, 1, units="ns").start())

    dut.rst.value = 1

    await Timer(5, units="ns")
    dut.rst.value = 0
    await RisingEdge(dut.clk)

    
    dut.i_addr0.value = 0
    dut.i_stb0.value = 1
    dut.i_cyc0.value = 1
    dut.i_we0.value = 0

    for i in range(10):
        data = sample(range(0, 0xFFFFFFFF), 65536)
        dut.mem.value = data
        await ClockCycles(dut.clk, 1)

        assert dut.mem.value == data
        
        await ClockCycles(dut.clk, 1)

        assert dut.o_data0.value == dut.mem[0].value

@cocotb.test(skip=True)
async def test_force_write_increment(dut):
    cocotb.start_soon(Clock(dut.clk, 1, units="ns").start())

    dut.rst.value = 1

    await Timer(5, units="ns")
    dut.rst.value = 0
    await RisingEdge(dut.clk)

    
    dut.i_addr0.value = 0
    dut.i_stb0.value = 1
    dut.i_cyc0.value = 1
    dut.i_we0.value = 0

    data = list(range(0, 65536))
    dut.mem.value = data[::-1]
    await ClockCycles(dut.clk, 1)

    assert dut.mem.value == data[::-1]
    
    for i in range(0, 65536):
        dut.i_addr0.value = i
        await ClockCycles(dut.clk, 2)
        if dut.o_data0.value != data[i]:
            breakpoint()
        #print(f"{i}: {int(dut.i_addr0.value)} : {int(dut.o_data0.value)}")


async def test_check_imem_ports_working(dut, port):
    cocotb.start_soon(Clock(dut.clk, 1, units="ns").start())

    data = list(range(0, 65536))
    dut.mem.value = data[::-1]

    await ClockCycles(dut.clk, 1)

    assert dut.mem.value == data[::-1]
    
    for i in range(0, 65536//4):
        if port == 0:
            dut.i_addr0.value = i << 2
        else:
            dut.i_addr1.value = i << 2
        
        await ClockCycles(dut.clk, 2)
        
        if port == 0:
            assert dut.o_data0.value == i
        else:
            assert dut.o_data1.value == i

# Parametrized tests
incrementing_tf = TestFactory(test_function=incrementing)
incrementing_tf.add_option(name="port", optionlist=[0, 1])
# incrementing_tf.generate_tests()

zeros_tf = TestFactory(test_function=zeros)
zeros_tf.add_option(name="port", optionlist=[0, 1])
# zeros_tf.generate_tests()

ones_tf = TestFactory(test_function=ones)
ones_tf.add_option(name="port", optionlist=[0, 1])
# ones_tf.generate_tests()

random_tf = TestFactory(test_function=random)
random_tf.add_option(name="port", optionlist=[0, 1])
# random_tf.generate_tests()

random_tf = TestFactory(test_function=test_check_imem_ports_working)
random_tf.add_option(name="port", optionlist=[0, 1])
random_tf.generate_tests()


