from glob import glob
import re
import subprocess

from pandas import DataFrame

vcd_path = "../sim/top_sim/parsed_power_vcd/*.vcd"

data = []

for vcd in glob(vcd_path):
    # TODO run command
    cmd = [
            "/tools/intelFPGA_pro/23.4/quartus/bin/quartus_pow", 
            "power_estimation",
            "-c", 
            "combit", 
            f"--input_vcd={vcd}"
    ]

    subprocess.run(cmd)

    with open("output_files/combit.pow.rpt") as f:
        lines = f.readlines()
    
    new_row = {
        "Test": vcd,
        "Total On-Chip Power Dissipation": 0,
        "I/O Standby On-Chip Power Dissipation": 0,
        "I/O Dynamic On-Chip Power Dissipation": 0,
        "Core Dynamic On-Chip Power Dissipation": 0,
        "Device Static On-Chip Power Dissipation": 0,
    }

    for line in lines:
        for key in new_row.keys():
            if match := re.search(rf"{key}.*?([\d.]*) mW", line):
                new_row[key] = float(match.group(1))

    data.append(new_row)


df = DataFrame(data)
df.to_csv("all_power_results.csv")