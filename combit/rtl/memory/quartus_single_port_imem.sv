// Quartus Prime Verilog Template
// True Dual Port RAM with single clock
//
// Read-during-write behavior is undefined for mixed ports 
// and "new data" on the same port

module quartus_single_port_imem
#(parameter DATA_WIDTH=32, parameter ADDR_WIDTH=16)
(
    input clk,
    input rst,
	input [(ADDR_WIDTH-1):0] i_addr0,
    input wire i_stb0,
    output reg o_ack0,
    input wire i_cyc0,
	output reg [(DATA_WIDTH-1):0] o_data0
);

	// Declare the RAM variable
	reg [DATA_WIDTH-1:0] mem[2**ADDR_WIDTH-1:0];

    initial 
	begin : INIT
		integer i;
		for(i = 0; i < 2**ADDR_WIDTH; i = i + 1)
			mem[i] = 32'b0;
	end

	// Port A 
	always @ (posedge clk)
	begin
		o_data0 <= mem[i_addr0[15:2]];
        
        o_ack0 <= i_stb0 & i_cyc0;
	end 


`ifndef VERILATOR
    initial begin
        $dumpfile("dump.vcd");
        $dumpvars(1, quartus_single_port_imem);
    end

    `ifdef MEM_TRACE
    generate
        genvar i;
        for(i=0; i<2**ADDR_WIDTH-1; i++)
        begin : MEM_TRACE
            wire [DATA_WIDTH-1:0] mem_trace;
            assign mem_trace = mem[i];
        end
    endgenerate
    `endif
`endif

endmodule