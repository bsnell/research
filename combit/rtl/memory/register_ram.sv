module register_ram #(
    parameter DATA_WIDTH = 32,
    parameter ADDR_WIDTH = 6,
    parameter RAM_DEPTH = (1 << ADDR_WIDTH)
)
(
    input wire clk,
    input wire rst,

    input wire [ADDR_WIDTH-1:0] i_waddr,
    input wire [DATA_WIDTH-1:0] i_wdata,
    input wire i_wen,
    input wire [ADDR_WIDTH-1:0] i_raddr,
    input wire i_ren,
    output reg [DATA_WIDTH-1:0] o_rdata
);

reg [DATA_WIDTH-1:0] mem [RAM_DEPTH-1:0];
integer i;

always @ (posedge clk) begin : RESET
    if (rst) begin
        for (i = 0; i < RAM_DEPTH-1; i++)
        begin
            mem[i] <= 0;
        end
    end
end

always @ (posedge clk) begin : WRITE
    if (i_wen & ~rst) begin
        mem[i_waddr] <= i_wdata;
    end
end

always @ (posedge clk) begin : READ
    if (i_ren & ~rst) begin
        o_rdata <= mem[i_raddr];
    end
end




`ifdef MEM_TRACE
generate
    genvar j;
    for(j=0; j<RAM_DEPTH; j++)
    begin : MEM_TRACE
        wire [DATA_WIDTH-1:0] mem_trace;
        assign mem_trace = mem[j];
    end
endgenerate
`endif


endmodule