module register_file (
	input clk, 
    input wen,
	input [5:0] waddr,
	input [5:0] raddr1,
	input [5:0] raddr2,
	input [31:0] wdata,
	output [31:0] rdata1,
	output [31:0] rdata2
);
	reg [31:0] regs [0:31];
	integer i;
	always @(posedge clk)
		if (wen) regs[waddr[4:0]] <= wdata;

	assign rdata1 = raddr1 ? regs[raddr1[4:0]] : 32'b0;
	assign rdata2 = raddr2 ? regs[raddr2[4:0]] : 32'b0;

    `ifdef MEM_TRACE
    generate
        genvar j;
        for(j=0; j<32; j++)
        begin : MEM_TRACE
            wire [31:0] mem_trace;
            assign mem_trace = regs[j];
        end
    endgenerate
`endif
endmodule