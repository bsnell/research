module dual_port_imem #(
    parameter DATA_WIDTH = 32,
    parameter ADDR_WIDTH = 16,
    parameter RAM_DEPTH = (1 << ADDR_WIDTH - 2)
)
(
    input wire clk,
    input wire rst,

    // Port 0
    input wire [ADDR_WIDTH-1:0] i_addr0,
    output reg [DATA_WIDTH-1:0] o_data0,
    // select is dropped for simplicity
    input wire i_stb0,
    output reg o_ack0,
    input wire i_cyc0,

    // Port 1
    input wire [ADDR_WIDTH-1:0] i_addr1,
    output reg [DATA_WIDTH-1:0] o_data1,
    // select is dropped for simplicity
    input wire i_stb1,
    output reg o_ack1,
    input wire i_cyc1
);

// NOTES:
// This memory does not have any arbitration
reg [DATA_WIDTH-1:0] mem[2**ADDR_WIDTH-1:0];
integer j;
always @ (posedge clk) begin : RESET
    if (rst) begin
        o_ack0 <= 0;
        o_ack1 <= 0;

        
        for(j=0; j<RAM_DEPTH; j++)
        begin : RESET_MEMORY
            mem[j] = 32'b0;
        end
    end else begin
        if (i_cyc0 & i_stb0) begin
            o_ack0 <= 1;
            
        end else begin
            o_ack0 <= 0;
        end
		  
		  o_data0 <= mem[i_addr0[15:2]];

        if (i_cyc1 & i_stb1) begin
            o_ack1 <= 1;
       
        end else begin
            o_ack1 <= 0;
        end
		  
		  o_data1 <= mem[i_addr1[15:2]];

    end
end


`ifndef VERILATOR
    initial begin
        $dumpfile("dump.vcd");
        $dumpvars(1, dual_port_imem);
    end

    `ifdef MEM_TRACE
    generate
        genvar i;
        for(i=0; i<RAM_DEPTH; i++)
        begin : MEM_TRACE
            wire [DATA_WIDTH-1:0] mem_trace;
            assign mem_trace = mem[i];
        end
    endgenerate
    `endif
`endif



endmodule