module quartus_dual_port_ram
#(parameter DATA_WIDTH=32, parameter ADDR_WIDTH=16)
(
	input [(DATA_WIDTH-1):0] i_data0, i_data1,
	input [(ADDR_WIDTH-1):0] i_addr0, i_addr1,
	input i_we0, i_we1, clk, rst,
	output reg [(DATA_WIDTH-1):0] o_data0, o_data1,
    input wire i_stb0,
    output reg o_ack0,
    input wire i_cyc0,
    input wire i_stb1,
    output reg o_ack1,
    input wire i_cyc1
);
	// Declare the mem variable
	reg [DATA_WIDTH-1:0] mem[2**ADDR_WIDTH-1:0];

    wire int_we0 = i_we0 & i_cyc0 & i_stb0;
    wire int_we1 = i_we1 & i_cyc1 & i_stb1;

	// Port A 
	always @ (posedge clk)
	begin
		if (int_we0) 
		begin
			mem[i_addr0[15:2]] = i_data0;
		end
		o_data0 <= mem[i_addr0[15:2]];
        o_ack0 = i_stb0 & i_cyc0;
	end 

	// Port B 
	always @ (posedge clk)
	begin
		if (int_we1) 
		begin
			mem[i_addr1[15:2]] = i_data1;
		end
		o_data1 <= mem[i_addr1[15:2]];
        o_ack1 = i_stb1 & i_cyc1;
	end

`ifndef VERILATOR
    initial begin
        $dumpfile("dump.vcd");
        $dumpvars(1, quartus_dual_port_ram);
    end

    `ifdef MEM_TRACE
    generate
        genvar i;
        for(i=0; i<2**ADDR_WIDTH-1; i++)
        begin : MEM_TRACE
            wire [DATA_WIDTH-1:0] mem_trace;
            assign mem_trace = mem[i];
        end
    endgenerate
    `endif
`endif
endmodule