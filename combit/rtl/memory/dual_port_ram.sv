module dual_port_ram #(
    parameter DATA_WIDTH = 32,
    parameter ADDR_WIDTH = 16,
    parameter RAM_DEPTH = (1 << (ADDR_WIDTH-2))
)
(
    input wire clk,
    input wire rst,

    // Port 0
    input wire [ADDR_WIDTH-1:0] i_addr0,
    input wire [DATA_WIDTH-1:0] i_data0,
    output reg [DATA_WIDTH-1:0] o_data0,
    input wire i_we0,
    // select is dropped for simplicity
    input wire i_stb0,
    output reg o_ack0,
    input wire i_cyc0,

    // Port 1
    input wire [ADDR_WIDTH-1:0] i_addr1,
    input wire [DATA_WIDTH-1:0] i_data1,
    output reg [DATA_WIDTH-1:0] o_data1,
    input wire i_we1,
    // select is dropped for simplicity
    input wire i_stb1,
    output reg o_ack1,
    input wire i_cyc1
);

// NOTES:
// This memory does not have any arbitration

reg [31:0] mem [RAM_DEPTH-1:0];
integer j;
always @ (posedge clk) begin : RESET
    if (i_we0 & i_cyc0 & i_stb0) begin
        o_ack0 <= 1;
        mem[i_addr0[15:2]] <= i_data0;
    end else if (!i_we0 & i_cyc0 & i_stb0) begin
        o_ack0 <= 1;
        o_data0 <= mem[i_addr0[15:2]];
    end else begin
        o_ack0 <= 0;
    end

    if (i_we1 & i_cyc1 & i_stb1) begin
        o_ack1 <= 1;
        mem[i_addr1[15:2]] <= i_data1;
    end else if (!i_we1 & i_cyc1 & i_stb1) begin
        o_ack1 <= 1;
        o_data1 <= mem[i_addr1[15:2]];
    end else begin
        o_ack1 <= 0;
    end
end


`ifndef ICARUS
    initial begin
        $dumpfile("dump.vcd");
        $dumpvars(1, dual_port_ram);
    end

    `ifdef MEM_TRACE
    generate
        genvar i;
        for(i=0; i<RAM_DEPTH; i++)
        begin : MEM_TRACE
            wire [DATA_WIDTH-1:0] mem_trace;
            assign mem_trace = mem[i];
        end
    endgenerate
    `endif
`endif


endmodule