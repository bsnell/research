`include "urv_defs.v"

`timescale 1ns/1ps

module rv_core_wishbone
  (
    input 	     clk_i,
    input 	     cpu_rst_i,
    input 	     rst_i,

    // data Wishbone bus
    output            d_cyc_o,
    output            d_stb_o,
    output            d_we_o,
    output reg [3:0]  d_sel_o,
    output reg [31:0] d_adr_o,
    output reg [31:0] d_dat_o,
    input [31:0]      d_dat_i,
    input 	     d_stall_i,
    input 	     d_ack_i,

    // instruction Wishbone bus
    output [31:0]       i_adr_o,
    output               i_cyc_o,
    input [31:0]            i_dat_i,
    input                   i_ack_i,

    input arbitrator_waiting,
    output arbitrator_ack,
    input [31:0] arbitrator_insn,
    input [31:0] arbitrator_rs1,
    input [31:0] arbitrator_rs2,
    output [31:0] arbitrator_rd,
    output arbitrator_valid
  );


  // data memory & Wishbone out
  reg [31:0] 	  dm_data_l;
  wire [3:0] 	  dm_data_select;
  wire 	  dm_load;
  wire 	  dm_store;

  wire		  dm_ready = 1;

  reg 		  dm_cycle_in_progress;

  reg [31:0] 	  dm_wb_rdata;


  assign d_cyc_o = dm_load || dm_store;
  assign d_stb_o = dm_load || dm_store;
  assign d_we_o = dm_store;
    
  urv_cpu cpu_core
          (
            .clk_i(clk_i),
            .rst_i(rst_i),

            .im_addr_o(i_adr_o),
            .im_data_i(i_dat_i),
            .im_valid_i(i_ack_i),
            .im_rd_o(i_cyc_o),

            .dm_addr_o(d_adr_o),
            .dm_data_s_o(d_dat_o),
            .dm_data_l_i(d_dat_i),
            .dm_data_select_o(d_sel_o),

            .dm_store_o(dm_store),
            .dm_load_o(dm_load),
            .dm_load_done_i(d_ack_i),
            .dm_store_done_i(d_ack_i),

            .dbg_force_i(1'b0),

            .arbitrator_waiting(arbitrator_waiting),
            .arbitrator_ack(arbitrator_ack),
            .arbitrator_insn(arbitrator_insn),
            .arbitrator_rs1(arbitrator_rs1),
            .arbitrator_rs2(arbitrator_rs2),
            .arbitrator_rd(arbitrator_rd),
            .arbitrator_valid(arbitrator_valid)
          );

endmodule



