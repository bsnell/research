module combit_top
(
    input wire clk,
    input wire cpu_rst,
    input wire imem_rst,
    input wire dmem_rst,
	 output wire little_imem_cyc,
	 output wire big_imem_cyc
//	 output wire little_dmem_stb,
//	 output wire big_dmem_stb,
//	 output wire [31:0] imem_little_data,
//	 output wire [31:0] imem_big_data,
//	 output wire [31:0] dmem_little_data,
//	 output wire [31:0] dmem_big_data
);

// Internal wires
// Format <source>_<dest>_<name>

// Little imem wires
wire [31:0] little_imem_addr;
wire [31:0] imem_little_data;
wire little_imem_stb;
wire imem_little_ack;
// wire little_imem_cyc;

// Big imem wires
wire [31:0] big_imem_addr;
wire [31:0] imem_big_data;
wire big_imem_stb;
wire imem_big_ack;
// wire big_imem_cyc;

// Little dmem wires
wire [31:0] little_dmem_addr;
wire [31:0] little_dmem_data;
wire [31:0] dmem_little_data;
wire little_dmem_we;
wire little_dmem_stb;
wire dmem_little_ack;
wire little_dmem_cyc;

// Extra wires for little core
wire [31:0] mem_little_data;
wire mem_little_ack;
wire [3:0] little_mem_sel;
wire little_imem_access;
wire little_dmem_cyc_calc;
wire little_dmem_stb_calc;

// Big dmem wires
wire [31:0] big_dmem_addr;
wire [31:0] big_dmem_data;
wire [31:0] dummy;
wire [31:0] dmem_big_data;
wire big_dmem_we;
wire big_dmem_stb;
wire dmem_big_ack;
wire big_dmem_cyc;

// little register wires
wire little_reg_wen;
wire [4:0] little_reg_waddr;
wire [4:0] little_reg_raddr1;
wire [4:0] little_reg_raddr2;
wire [31:0] little_reg_wdata;
wire [31:0] reg_little_rdata1;
wire [31:0] reg_little_rdata2;

// little arbitrator wires
wire little_arbit_pcpi_valid;
wire [31:0] little_arbit_pcpi_insn;
wire [31:0] little_arbit_pcpi_rs1;
wire [31:0] little_arbit_pcpi_rs2;
wire arbit_little_pcpi_wr;
wire [31:0] arbit_little_pcpi_rd;
wire arbit_little_pcpi_wait;
wire arbit_little_pcpi_ready;

// big arbitrator wires
wire arbit_big_waiting;
wire big_arbit_ack;
wire [31:0] arbit_big_insn;
wire [31:0] arbit_big_rs1;
wire [31:0] arbit_big_rs2;
wire [31:0] big_arbit_rd;
wire big_arbit_valid;


/*synthesis keep*/ quartus_single_port_imem little_imem
    (
        .clk(clk),
        .rst(imem_rst),

        // Port 0
        .i_addr0(little_imem_addr[15:0]),
        .o_data0(imem_little_data),
        .i_stb0(little_imem_cyc),  // Little doesn't actually provide a strobe, this may need tied to the cycle
        .o_ack0(imem_little_ack),
        .i_cyc0(little_imem_cyc)

    );

/*synthesis keep*/ quartus_single_port_imem big_imem
    (
        .clk(clk),
        .rst(imem_rst),

        // Port 0
        .i_addr0(big_imem_addr[15:0]),
        .o_data0(imem_big_data),
        .i_stb0(big_imem_cyc),
        .o_ack0(imem_big_ack),
        .i_cyc0(big_imem_cyc)

    );

/*synthesis keep*/ quartus_dual_port_ram dmem
    (
        .clk(clk),
        .rst(dmem_rst),

        // Port 0
        .i_addr0(little_dmem_addr[15:0]),
        .i_data0(little_dmem_data),
        .o_data0(dmem_little_data),
        .i_we0(little_dmem_we),
        .i_stb0(little_dmem_cyc_calc),
        .o_ack0(dmem_little_ack),
        .i_cyc0(little_dmem_stb_calc),

        // Port 1
        .i_addr1(big_dmem_addr[15:0]),
        .i_data1(big_dmem_data),
        .o_data1(dmem_big_data),
        .i_we1(big_dmem_we),
        .i_stb1(big_dmem_stb),
        .o_ack1(dmem_big_ack),
        .i_cyc1(big_dmem_cyc)
    );

assign little_imem_addr = little_dmem_addr;
assign mem_little_data = little_imem_access ? imem_little_data : dmem_little_data;
assign mem_little_ack = (imem_little_ack & little_imem_access) | (dmem_little_ack & !little_imem_access);
assign little_imem_cyc = little_dmem_stb & little_imem_access;
assign little_dmem_cyc_calc = little_dmem_cyc & !little_imem_access;
assign little_dmem_stb_calc = little_dmem_stb & !little_imem_access;

picorv32_wb #(
    .ENABLE_PCPI(1),
    .REGS_INIT_ZERO(1)
    ) 
    little 
    (
        .wb_rst_i(cpu_rst),
        .wb_clk_i(clk),

        .wbm_adr_o(little_dmem_addr),
        .wbm_dat_o(little_dmem_data),
        .wbm_dat_i(mem_little_data),
        .wbm_we_o(little_dmem_we),
        .wbm_sel_o(little_mem_sel),
        .wbm_stb_o(little_dmem_stb),
        .wbm_ack_i(mem_little_ack),
        .wbm_cyc_o(little_dmem_cyc),

        .pcpi_valid(little_arbit_pcpi_valid),
        .pcpi_insn(little_arbit_pcpi_insn),
        .pcpi_rs1(little_arbit_pcpi_rs1),
        .pcpi_rs2(little_arbit_pcpi_rs2),
        .pcpi_wr(arbit_little_pcpi_wr),
        .pcpi_rd(arbit_little_pcpi_rd),
        .pcpi_wait(arbit_little_pcpi_wait),
        .pcpi_ready(arbit_little_pcpi_ready),

        .irq(),
        .eoi(),

        .trace_valid(),
        .trace_data(),

        .mem_instr(little_imem_access)
    );

arbitrator arbitrator
    (
        .clk(clk),
        .i_rst(cpu_rst),

        // Little interface
        .pcpi_valid(little_arbit_pcpi_valid),
        .pcpi_insn(little_arbit_pcpi_insn),
        .pcpi_rs1(little_arbit_pcpi_rs1),
        .pcpi_rs2(little_arbit_pcpi_rs2),
        .pcpi_wr(arbit_little_pcpi_wr),
        .pcpi_rd(arbit_little_pcpi_rd),
        .pcpi_wait(arbit_little_pcpi_wait),
        .pcpi_ready(arbit_little_pcpi_ready),

        // Big interface
        .arbitrator_waiting(arbit_big_waiting),
        .arbitrator_ack(big_arbit_ack),
        .arbitrator_insn(arbit_big_insn),
        .arbitrator_rs1(arbit_big_rs1),
        .arbitrator_rs2(arbit_big_rs2),
        .arbitrator_rd(big_arbit_rd),
        .arbitrator_valid(big_arbit_valid)
    );

rv_core_wishbone big
    (
        .clk_i(clk),
        .cpu_rst_i(cpu_rst),  // TODO it may be necessary to figure out what each of these do
        .rst_i(cpu_rst),

        // Data memory
        .d_cyc_o(big_dmem_cyc),
        .d_stb_o(big_dmem_stb),
        .d_we_o(big_dmem_we),
        .d_sel_o(),  // Left open
        .d_adr_o(big_dmem_addr),
        .d_dat_o(big_dmem_data),
        .d_dat_i(dmem_big_data),
        .d_stall_i(1'b0),  // Tied to zero
        .d_ack_i(dmem_big_ack),
        
        .i_adr_o(big_imem_addr),
        .i_cyc_o(big_imem_cyc),
        .i_dat_i(imem_big_data),
        .i_ack_i(imem_big_ack),
        .arbitrator_waiting(arbit_big_waiting),
        .arbitrator_ack(big_arbit_ack),
        .arbitrator_insn(arbit_big_insn),
        .arbitrator_rs1(arbit_big_rs1),
        .arbitrator_rs2(arbit_big_rs2),
        .arbitrator_rd(big_arbit_rd),
        .arbitrator_valid(big_arbit_valid)
    );

`ifndef ICARUS
  initial begin
    $dumpfile("dump.vcd");
    $dumpvars(10, combit_top);
  end
`endif
endmodule