module arbitrator (
	input clk, 
    input i_rst,
	
    // Little interface
    input pcpi_valid,
    input [31:0] pcpi_insn,
    input [31:0] pcpi_rs1,
    input [31:0] pcpi_rs2,
    output reg pcpi_wr,
    output reg [31:0] pcpi_rd,
    output reg pcpi_wait,
    output reg pcpi_ready,

    // Big interface
    output reg arbitrator_waiting,
    input arbitrator_ack,
    output [31:0] arbitrator_insn,
    output [31:0] arbitrator_rs1,
    output [31:0] arbitrator_rs2,
    input [31:0] arbitrator_rd,
    input arbitrator_valid
);
	
    // Pass through signals (latched by little processor)
    assign arbitrator_insn = pcpi_insn;
    assign arbitrator_rs1 = pcpi_rs1;
    assign arbitrator_rs2 = pcpi_rs2;
    
    typedef enum { IDLE, WAITING_SERVICE, SERVICE_STARTED, SERVICE_COMPLETE } arbitrator_state;
    arbitrator_state state;

    always @ (posedge clk) begin
        if (i_rst) begin
            state <= IDLE;
        end

        else begin
            case (state)
                IDLE: begin
                    if (pcpi_valid) begin
                        state <= WAITING_SERVICE;
                        arbitrator_waiting <= 1;
                        pcpi_wait <= 1;
                    end

                    else begin
                        arbitrator_waiting <= 0;
                        pcpi_wait <= 0;
                    end
                end

                WAITING_SERVICE: begin
                    if (arbitrator_ack) begin
                        state <= SERVICE_STARTED;
                        arbitrator_waiting <= 0;
                    end
                end

                SERVICE_STARTED: begin
                    if (arbitrator_valid) begin
                        state <= SERVICE_COMPLETE;
                        pcpi_wait <= 0;
                        pcpi_rd <= arbitrator_rd;
                        pcpi_wr <= 1;
                        pcpi_ready <= 1;
                    end
                end

                SERVICE_COMPLETE: begin
                    state <= IDLE;
                    pcpi_ready <= 0;
                end

                default: begin
                    state <= IDLE;
                end
            endcase
        end
    end
endmodule