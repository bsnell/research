Setting up Sim

0) Create a python virtual environment if you wish. I'm building with Python 3.10
`python3.10 -m venv sim`

1) Install the prerequisites for CocoTB. I could outline them, but this guide is better.
https://docs.cocotb.org/en/stable/install.html#install-prerequisites

2) Install Icarus Verilog. I'm using version 11.0. If I recall this required a bit of work on my Ubuntu LTS setup
https://steveicarus.github.io/iverilog/

3) Install Gtkwave
https://gtkwave.sourceforge.net/

4) Install the required python packages 
`cd sim`
`pip install -r requirements.txt`

5) Install the rv32im toolchain. I followed this guide. A few notes. Doing the submodule init without more "jobs" will be painful. `--jobs 16`. Some of them fail, but they weren't important for what I needed. I put the rv32im toolchain specifically in `/opt/riscv32im`. You'll also need to add that to your path. `export PATH=$PATH:/opt/riscv32im` Granted this will require superuser priviledges. 

https://github.com/YosysHQ/picorv32#building-a-pure-rv32i-toolchain

6) Run the tests `make` possibly `make clean` if you have changes that you want to confirm make it in.

7) Open the waves with Gtkwave
`gtkwave dump.vcd`